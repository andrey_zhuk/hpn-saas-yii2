<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patients_diagnosis".
 *
 * @property integer $id
 * @property integer $patient_id
 * @property string $primary_diagnosis
 * @property integer $date_primary_diagnosis
 * @property string $secondary_diagnosis
 * @property integer $date_secondary_diagnosis
 * @property string $necessary_home_services
 */
class PatientsDiagnosis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patients_diagnosis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['primary_diagnosis', 'necessary_home_services'], 'required'],
            [['date_primary_diagnosis', 'date_secondary_diagnosis'], 'integer'],
            [['primary_diagnosis', 'secondary_diagnosis'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'primary_diagnosis' => 'Primary Diagnosis',
            'date_primary_diagnosis' => 'Date Primary Diagnosis',
            'secondary_diagnosis' => 'Secondary Diagnosis',
            'date_secondary_diagnosis' => 'Date Secondary Diagnosis',
            'necessary_home_services' => 'Necessary Home Services',
        ];
    }

    /**
     * @inheritdoc
     * @return PatientsDiagnosisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatientsDiagnosisQuery(get_called_class());
    }

    public function afterFind()
    {
        $this->necessary_home_services = json_decode($this->necessary_home_services);
        $this->date_primary_diagnosis = date('m/d/Y', $this->date_primary_diagnosis);
        $this->date_secondary_diagnosis = date('m/d/Y', $this->date_secondary_diagnosis);

        parent::afterFind();
    }
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->necessary_home_services = json_encode($this->necessary_home_services);

            return true;
        } else {
            return false;
        }
    }
}
