<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrdersRequest]].
 *
 * @see OrdersRequest
 */
class OrdersRequestQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OrdersRequest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrdersRequest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}