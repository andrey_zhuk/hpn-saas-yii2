<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan_care".
 *
 * @property integer $id
 * @property integer $id_patient
 * @property string $claim
 * @property integer $start_care_date
 * @property integer $certification_period_start
 * @property integer $certification_period_finish
 * @property string $medical_record
 * @property string $provider_no
 * @property string $medications
 * @property string $principal_icd
 * @property string $principal
 * @property integer $principal_date
 * @property string $procedure_icd
 * @property string $procedure
 * @property integer $procedure_date
 * @property string $other_diagnoses_icd
 * @property string $other_pertinent_diagnoses
 * @property integer $other_diagnoses_date
 * @property string $dme_supplies
 * @property string $safety_measures
 * @property string $nutritional_req
 * @property string $allergies
 * @property string $functional_limitations
 * @property string $functional_limitations_other
 * @property string $activities_permitted
 * @property string $activities_permitted_other
 * @property string $mental_status
 * @property integer $prognosis
 * @property string $treatments
 * @property string $discharge_plans
 */
class PlanCare extends \yii\db\ActiveRecord
{

    public $prognosis_array = ['poor', 'guarded', 'fair', 'good', 'excellent'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_care';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['claim', 'start_care_date', 'certification_period_start', 'certification_period_finish', 'medical_record', 'provider_no', 'medications', 'principal_icd', 'principal', 'principal_date', 'procedure_icd', 'procedure', 'procedure_date', 'other_diagnoses_icd', 'other_pertinent_diagnoses', 'other_diagnoses_date', 'dme_supplies', 'safety_measures', 'nutritional_req', 'allergies', 'functional_limitations', 'activities_permitted', 'mental_status', 'prognosis', 'treatments', 'discharge_plans'], 'required'],
            [['prognosis'], 'integer'],
            [['treatments', 'discharge_plans'], 'string'],
            [['claim', 'medical_record', 'provider_no'], 'string', 'max' => 50],
            [['medications', 'principal', 'procedure', 'other_pertinent_diagnoses', 'dme_supplies', 'safety_measures', 'nutritional_req', 'allergies'], 'string', 'max' => 255],
            [['principal_icd', 'procedure_icd', 'other_diagnoses_icd'], 'string', 'max' => 11],
            [['functional_limitations_other', 'activities_permitted_other'], 'string', 'max' => 100]
        ];
    }
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $start_care_date = explode('/', $this->start_care_date);
            $this->start_care_date = gmmktime(0,0,0,$start_care_date[0],$start_care_date[1],$start_care_date[2]);
            $cp_start = explode('/', $this->certification_period_start);
            $this->certification_period_start = gmmktime(0,0,0,$cp_start[0],$cp_start[1],$cp_start[2]);
            $cp_finish = explode('/', $this->certification_period_finish);
            $this->certification_period_finish = gmmktime(0,0,0,$cp_finish[0],$cp_finish[1],$cp_finish[2]);
            $principal_date = explode('/', $this->principal_date);
            $this->principal_date = gmmktime(0,0,0,$principal_date[0],$principal_date[1],$principal_date[2]);
            $procedure_date = explode('/', $this->procedure_date);
            $this->procedure_date = gmmktime(0,0,0,$procedure_date[0],$procedure_date[1],$procedure_date[2]);
            $other_diagnoses_date = explode('/', $this->other_diagnoses_date);
            $this->other_diagnoses_date = gmmktime(0,0,0,$other_diagnoses_date[0],$other_diagnoses_date[1],$other_diagnoses_date[2]);

            $this->functional_limitations = json_encode($this->functional_limitations);
            $this->activities_permitted = json_encode($this->activities_permitted);
            $this->mental_status = json_encode($this->mental_status);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->start_care_date = date('m/d/Y', $this->start_care_date);
        $this->certification_period_start = date('m/d/Y', $this->certification_period_start);
        $this->certification_period_finish = date('m/d/Y', $this->certification_period_finish);
        $this->principal_date = date('m/d/Y', $this->principal_date);
        $this->procedure_date = date('m/d/Y', $this->procedure_date);
        $this->other_diagnoses_date = date('m/d/Y', $this->other_diagnoses_date);

        $this->functional_limitations = json_decode($this->functional_limitations);
        $this->activities_permitted = json_decode($this->activities_permitted);
        $this->mental_status = json_decode($this->mental_status);

        parent::afterFind();
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_patient' => Yii::t('app', 'Id Patient'),
            'claim' => Yii::t('app', 'Claim'),
            'start_care_date' => Yii::t('app', 'Start Care Date'),
            'certification_period_start' => Yii::t('app', 'Certification Period Start'),
            'certification_period_finish' => Yii::t('app', 'Certification Period Finish'),
            'medical_record' => Yii::t('app', 'Medical Record'),
            'provider_no' => Yii::t('app', 'Provider No'),
            'medications' => Yii::t('app', 'Medications'),
            'principal_icd' => Yii::t('app', 'Principal Icd'),
            'principal' => Yii::t('app', 'Principal'),
            'principal_date' => Yii::t('app', 'Principal Date'),
            'procedure_icd' => Yii::t('app', 'Procedure Icd'),
            'procedure' => Yii::t('app', 'Procedure'),
            'procedure_date' => Yii::t('app', 'Procedure Date'),
            'other_diagnoses_icd' => Yii::t('app', 'Other Diagnoses Icd'),
            'other_pertinent_diagnoses' => Yii::t('app', 'Other Pertinent Diagnoses'),
            'other_diagnoses_date' => Yii::t('app', 'Other Diagnoses Date'),
            'dme_supplies' => Yii::t('app', 'Dme Supplies'),
            'safety_measures' => Yii::t('app', 'Safety Measures'),
            'nutritional_req' => Yii::t('app', 'Nutritional Req'),
            'allergies' => Yii::t('app', 'Allergies'),
            'functional_limitations' => Yii::t('app', 'Functional Limitations'),
            'functional_limitations_other' => Yii::t('app', 'Functional Limitations Other'),
            'activities_permitted' => Yii::t('app', 'Activities Permitted'),
            'activities_permitted_other' => Yii::t('app', 'Activities Permitted Other'),
            'mental_status' => Yii::t('app', 'Mental Status'),
            'prognosis' => Yii::t('app', 'Prognosis'),
            'treatments' => Yii::t('app', 'Treatments'),
            'discharge_plans' => Yii::t('app', 'Discharge Plans'),
        ];
    }

    /**
     * @inheritdoc
     * @return PlanCareQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlanCareQuery(get_called_class());
    }
}
