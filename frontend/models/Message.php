<?php

namespace app\models;

use common\models\Agencys;
use common\models\Physician;
use dektrium\user\models\Profile;
use common\models\User;
use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $message
 * @property integer $date_create
 * @property string $status
 * @property integer $date_read
 *
 * @property Chats $chat
 * @property UserMessages[] $userMessages
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'message'], 'required'],
            [['chat_id', 'status'], 'integer'],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'message' => Yii::t('app', 'Message'),
            'date_create' => Yii::t('app', 'Date Create'),
            'status' => Yii::t('app', 'Status'),
            'date_read' => Yii::t('app', 'Date Read'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chats::className(), ['id' => 'chat_id'])->with(['members']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(UserMessages::className(), ['message_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(UserMessages::className(), ['message_id' => 'id'])
                    ->where(['recipient' => \Yii::$app->user->id]);
    }
}
