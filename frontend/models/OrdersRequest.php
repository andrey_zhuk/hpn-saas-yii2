<?php

namespace app\models;

use common\models\Agencys;
use common\models\Patients;
use common\models\Physician;
use dektrium\user\models\Profile;
use Yii;

/**
 * This is the model class for table "orders_request".
 *
 * @property integer $id
 * @property integer $id_patient
 * @property integer $id_agency
 * @property string $text
 * @property integer $date_create
 * @property string $type
 * @property integer $date_read_physician
 * @property integer $date_resend
 * @property integer $date_read_agency
 * @property integer $date_accept
 */
class OrdersRequest extends \yii\db\ActiveRecord
{
    const PHYSICIAN_SEND = '0';
    const AGENCY_SEND = '1';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_patient', 'id_agency', 'text', 'date_create'], 'required'],
            [['id_patient', 'id_agency', 'date_create', 'date_read_physician', 'date_resend', 'date_read_agency', 'date_accept'], 'integer'],
            [['type'], 'string'],
            [['text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_patient' => Yii::t('app', 'Id Patient'),
            'id_agency' => Yii::t('app', 'Id Agency'),
            'text' => Yii::t('app', 'Text'),
            'date_create' => Yii::t('app', 'Date Create'),
            'type' => Yii::t('app', 'Type'),
            'date_read_physician' => Yii::t('app', 'Date Read Physician'),
            'date_resend' => Yii::t('app', 'Date Resend'),
            'date_read_agency' => Yii::t('app', 'Date Read Agency'),
            'date_accept' => Yii::t('app', 'Date Accept'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrdersRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrdersRequestQuery(get_called_class());
    }

//    public function afterFind()
//    {
//        if ($this->date_create != NULL)
//            $this->date_create = date('m/d/Y', (int)$this->date_create);
//        if ($this->date_read_physician != NULL)
//            $this->date_read_physician = date('m/d/Y', (int)$this->date_read_physician);
//        if ($this->date_resend != NULL)
//            $this->date_resend = date('m/d/Y', (int)$this->date_resend);
//        if ($this->date_read_agency != NULL)
//            $this->date_read_agency = date('m/d/Y', (int)$this->date_read_agency);
//        if ($this->date_accept != NULL)
//            $this->date_accept = date('m/d/Y', (int)$this->date_accept);
//
//        parent::afterFind();
//    }

    public function getPatients()
    {
        return $this->hasOne(Patients::className(), ['id' => 'id_patient']);
    }

    public function getAgency()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency']);
    }

    public function getPhysician()
    {
        return $this->hasOne(Physician::className(), ['id' => 'id_physician'])
            ->viaTable(Patients::tableName(), ['id' => 'id_patient']);
    }
}
