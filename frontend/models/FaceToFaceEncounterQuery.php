<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FaceToFaceEncounter]].
 *
 * @see FaceToFaceEncounter
 */
class FaceToFaceEncounterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FaceToFaceEncounter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FaceToFaceEncounter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}