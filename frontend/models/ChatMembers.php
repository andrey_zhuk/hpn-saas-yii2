<?php

namespace app\models;

use common\models\LoginData;
use Yii;
use common\models\User;
use common\models\Agencys;
use common\models\Physician;

/**
 * This is the model class for table "chat_members".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $chat_id
 *
 * @property Chats $chat
 * @property User $user
 */
class ChatMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'chat_id'], 'required'],
            [['user_id', 'chat_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chats::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(LoginData::className(), ['id' => 'user_id']);
//        return $this->hasOne(key(\Yii::$app->authManager->getRolesByUser($this->user_id)) == 'agency' ? Agencys::className() :  Physician::className(),
//                                ['id' => 'user_id'])
//                    ->select('*');
    }
}
