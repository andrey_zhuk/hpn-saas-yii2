<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chats".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ChatMembers[] $chatMembers
 */
class Chats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(ChatMembers::className(), ['chat_id' => 'id'])->with(['user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['chat_id' => 'id'])->with(['sender', 'recipient'])
                    ->innerJoin('user_messages', 'user_messages.recipient = ' .  \Yii::$app->user->id . ' AND
                                user_messages.message_id = message.id');
    }


    /**
     * Get latest members(users)
     *
     * @return mixed
     */
    public function getLatestMembers()
    {
        return ChatMembers::find()->where(['user_id' => \Yii::$app->user->id])->with(['user'])->all();
    }
}
