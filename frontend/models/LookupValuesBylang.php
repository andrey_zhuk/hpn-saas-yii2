<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_values_bylang".
 *
 * @property string $ref
 * @property string $lang
 * @property string $code
 * @property string $title
 * @property string $description
 * @property integer $sort
 */
class LookupValuesBylang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_values_bylang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ref', 'lang', 'code'], 'required'],
            [['sort'], 'integer'],
            [['ref'], 'string', 'max' => 20],
            [['lang'], 'string', 'max' => 2],
            [['code'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ref' => Yii::t('app', 'Ref'),
            'lang' => Yii::t('app', 'Lang'),
            'code' => Yii::t('app', 'Code'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }
}
