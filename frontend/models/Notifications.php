<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $id_from
 * @property integer $id_to
 * @property string $text
 * @property string $link
 * @property integer $date_send
 * @property string $status_read
 * @property integer $date_read
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_from', 'id_to', 'text', 'link', 'date_send'], 'required'],
            [['id_from', 'id_to', 'date_send', 'date_read'], 'integer'],
            [['status_read'], 'string'],
            [['text', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_from' => Yii::t('app', 'Id From'),
            'id_to' => Yii::t('app', 'Id To'),
            'text' => Yii::t('app', 'Text'),
            'link' => Yii::t('app', 'Link'),
            'date_send' => Yii::t('app', 'Date Send'),
            'status_read' => Yii::t('app', 'Status Read'),
            'date_read' => Yii::t('app', 'Date Read'),
        ];
    }

    /**
     * @inheritdoc
     * @return NotificationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NotificationsQuery(get_called_class());
    }
}
