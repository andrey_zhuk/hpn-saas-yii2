<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
        '@nineinchnick/nfy' => '@vendor/nineinchnick/yii2-nfy',
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\LoginData',
            'enableAutoLogin' => true,
//            'identityCookie' => [
//                'name' => '_frontendUser', // unique for frontend
//                'path'=>'/frontend'  // correct path for the frontend app.
//            ],
            'loginUrl'=>['/']
        ],
        'view' => [
            'theme' => [
                'baseUrl' => '@app/themes/saas',
                'pathMap' => ['@app/views' => '@app/themes/saas'],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'formatter' => [
            'currencyCode' => "EUR",
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => [
//                '<controller:\w+>/<id:\d+>' => '<controller>/view',
//                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

//                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
//                'portfolio/settings/<action:\w+>' => 'portfolio/<action>',
//                'launch/brief/<slug:[-a-zA-Z]+>-<id:\d+>' => 'launch/brief'
                'patient/new-patient-referral-form/<shortId:[\w-]+>' => 'patient/new-patient-referral-form',
                'profile/show/<shortId:[\w-]+>' => 'profile/show',
                'message/index/<shortId:[\w-]+>' => 'message/index',
                'patient/index/<id:\w+>' => 'patient/index'
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'error/handle',
        ],
        'dbmq' => [
            'class' => 'nineinchnick\nfy\components\DbQueue',
            'id' => 'queue',
            'label' => 'Notifications',
            'timeout' => 30,
        ],
        'sysvmq' => [
            'class' => 'nineinchnick\nfy\components\SysVQueue',
            'id' => 'a',
            'label' => 'IPC queue',
        ],
        'redismq' => [
            'class' => 'nineinchnick\nfy\components\RedisQueue',
            'id' => 'mq',
            'label' => 'Redis queue',
            'redis' => 'redis',
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'gii' => [
            'class' => 'yii\gii\Module'
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'nfy' => [
            'class' => 'nineinchnick\nfy\Module',
        ],
    ],
    'params' => $params,
    'name' => 'SaaS',
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.127', '192.168.0.212', '178.62.195.194']
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;