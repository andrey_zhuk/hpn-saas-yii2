'use strict';
var chatApp = angular.module("ChatApp", ['angular-smilies','mgcrea.ngStrap'])
    .directive('scrollMessages', function () { /* for scrolling */
        return {
            scope: {
                scrollMessages: "=",
                onScroll: "=",
                readMsg: "=",
                chat: "="
            },
            link: function (scope, element, attr) {
                /* auto scroll messages */
                scope.$watchCollection('scrollMessages', function (newValue) {
                    if (newValue && scope.chat != undefined && scope.chat.new_messages == 0) {
                        $(element).scrollTop($(element)[0].scrollHeight);
                    }
                });

                /* auto read message */
                element.on("scroll", function () {
                    var item = angular.element(document.getElementsByClassName(attr.onScroll));
                    //var item = angular.element(element);
                    var wH = element[0].offsetHeight;
                    var wS = element[0].scrollTop;
                    var message = {};
                    angular.forEach(item, function (value) {
                        var height = value.offsetHeight;
                        var pos = value.offsetTop;
                        var res = height + pos - wS;
                        if (wH >= res) {
                            $(value).removeClass('unread', 2000, 'easeOutElastic');
                            scope.readMsg($(value).data('message'));
                        }
                    });
                });
            }
        }
    }).directive('contenteditable', function() {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function(scope, element, attr, ngModel) {
                var read;
                if (!ngModel) {
                    return;
                }
                ngModel.$render = function() {
                    return element.html(ngModel.$viewValue);
                };
                element.bind('blur', function() {
                    if (ngModel.$viewValue !== $.trim(element.html())) {
                        return scope.$apply(read);
                    }
                });
                return read = function() {
                    return ngModel.$setViewValue($.trim(element.html()));
                };
            }
        };
    });

chatApp.run(function($http) {
    $http.defaults.headers.post = {'Content-Type': 'application/x-www-form-urlencoded'};
    $http.defaults.transformRequest = function(obj) {
        var str = [];
        for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    };
});

chatApp.controller("ChatController",  ['$scope', '$http', function ($scope, $http) {
    $scope.unreadMessages = {};
    $scope.chat = {};
    $scope.page = 1;
    $scope.typing_users = [];
    /* private chats with each user */
    $scope.chats = [];
    $scope.fetching = false;

    var typing = false;
    var timeout = undefined;

    $scope.loadHistoryChat = function(chat_id){
        $scope.chat.fetching = true;
        var parameters = {
            chat_id: chat_id
        };
        var config = {
            params: parameters
        };
        $http.get("/api/load-chat-history", config)
            .then(function(response) {
                socket.emit('initChat', response.data.id, current_user);
                $scope.chat = response.data;
                $scope.chat.page = 1;
                $scope.chat.fetching = false;
                if(response.data.messages.length > 0) {
                    /* for pagination */
                    $scope.chat.message_id = response.data.messages[0].id;
                }
                $scope.new_messages = response.data.new_messages;
                $scope.openSmiley = false;
                $scope.chat.message = '';
                //$scope.chat.edit_message = {};
            });
    };

    $scope.getUrlPrefix = function($file){
        if($file){
            return '/app/images/avatar/'+$file;
        } else {
            return '/app/images/noavatar.png';
        }
    };
    /**
     * Send message
     */
    $scope.sendMessage = function () {
        var parameters = {
            message: $scope.chat.message,
            chat_id: $scope.chat.id
        };
        var config = {
            params: parameters
        };
        $http.get("/message/save-message", config)
            .then(function(response) {
                if(response.errors) {
                    showMsg('warning', 'You can\'t send message');
                    return false;
                }
                /* reset input message */
                $scope.chat.message = '';
                /* send message to all member in chat */
                socket.emit('send chat message', response.data);
            });
    };

    /**
     * Send private message(in popup)
     */
    $scope.sendPrivateMessage = function (chat) {
        $scope.sendMessage(chat);
        showMsg('success', 'Message was sent!');
        $('#sendMessage').modal('hide');
    };

    /**
     * Receive message
     */
    socket.on('send-chat-message', function (data) {
        console.log(data);
        //console.log('send-chat-message');
        //console.log(data);
        /* if isset any chat then push to it */
        if ($scope.chat.messages) {
            $scope.chat.messages.push(data);
            $scope.readMessage(data);
        } /* show info msg */
        else {
            showMsg('info', 'New message',
                            '<a href="/message?id=' + data.sender.user.id_user + '">' + data.sender.user.name  + '</a> ' +
                            '<p> ' + data.message + '</p>');
            $scope.unreadMessages.count++;
        }
        $scope.$apply();
    });

    /**
     * Receive updated message
     */
    socket.on('update-chat-message', function (data) {
        /* find message in chat message */
        var message = _.find($scope.chat.messages, function(message) {
            return message.id == data.id;
        });
        if(message != undefined) {
            message.message = data.message;
        }
        $scope.$apply();
    });

    /**
     * Current user is press key
     */
    $scope.userIsTyping = function (chat_id) {
        if(typing == false) {
            typing = true
            startTyping(chat_id);
            timeout = setTimeout(function() {
                stopTyping(chat_id);
            }, 3000);
        } else {
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                stopTyping(chat_id);
            }, 3000);
        }
    };


    /**
     * Listen if user typing
     */
    socket.on("is-typing", function (data) {
        if (data.typing) {
            timeout = setTimeout(function() {
                stopTyping(data.chat_id);
            }, 1000);
        }
        $scope.typing_users = data.users;
        $scope.$apply();
    });

    /**
     * Start user typing
     */
    function startTyping(chat_id) {
        socket.emit("typing", {typing: true, user: current_user, chat_id: chat_id });
    }

    /**
     * Stop user typing
     */
    function stopTyping(chat_id) {
        typing = false;
        //$scope.typing_users = [];
        socket.emit("typing", {typing: false, user: current_user, chat_id: chat_id });
    }

    /**
     * Show older messages
     */
    $scope.loadPreviousMessages = function (chat, page, limit) {
        var parameters = {
            message_id: chat.message_id,
            chat_id: chat.id,
            limit: limit
        };
        var config = {
            params: parameters
        };
        $http.get("/api/load-chat-history", config)
            .then(function(response) {
                if(response.data.messages.length !== 0) {
                    chat.message_id = response.data.messages[0].id;
                    chat.messages.unshift.apply($scope.chat.messages, response.data.messages);
                } else {
                    chat.noMessages = true;
                }
            });
    };

    /**
     * if user hover on message
     */
    $scope.hoverInMessage = function(){
        this.buttonDeleteMessage = true;
    };

    $scope.hoverOutMessage = function(){
        this.buttonDeleteMessage = false;
    };

    /**
     * Remove the message
     */
    $scope.removeMessage = function(message){
        var parameters = {
            message_id: message.id
        };
        var config = {
            params: parameters
        };
        $http.get("/message/remove-message", config)
            .then(function(response) {
                showMsg('success', 'Message was deleted!');
                var index = $scope.chat.messages.indexOf(message);
                $scope.chat.messages.splice(index, 1);
                if($scope.chat.messages.length < 10)
                    $scope.loadPreviousMessages($scope.chat, 1, 1);
            });
    };

    /**
     * Edit message
     *
     * @param message
     */
    $scope.editMessage = function(message) {
        $scope.chat.message = message.message;
        $scope.chat.edit_message = {
            id: message.id
        }
    };

    /**
     * Cancel edit message
     */
    $scope.cancelEditMessage = function () {
        $scope.chat.message = '';
        $scope.chat.edit_message = {};
    };

    /**
     * Update message
     * @param message
     */
    $scope.updateMessage = function() {
        var parameters = {
            text: $scope.chat.message,
            message_id: $scope.chat.edit_message.id
        };
        var temp_obj = {};
        var config = {
            params: parameters
        };

        $http.get("/message/update-message", config)
            .then(function(response) {
                _.each($scope.chat.messages, function(val, num) {
                    if (val.id == response.data.id) {
                        angular.copy($scope.chat.messages[num], temp_obj);
                        temp_obj.message = response.data.message;
                        $scope.chat.messages[num] = temp_obj;
                    }
                });
                showMsg('success', 'Message was updated!');
                /* reset input message */
                $scope.cancelEditMessage();
                /* update message in all member in chat */
                socket.emit('update chat message', response.data);
            });
    };

    /**
     * if user key press on input
     */
    $scope.keyPressOnInputMessage = function($event){
        if($event.charCode != 13)
            $scope.userIsTyping($scope.chat.id);

        if($event.charCode == 13)
            $scope.sendMessage($scope.chat);
    };
    /**
     * Check if is mine message
     */
    $scope.checkMineMessage = function (message) {
        return message.sender.sender == current_user.user_id;
    };


    /**
     * Read the message
     */
    $scope.readMessage = function(message){
        if((message.recipient != undefined && message.recipient.read == 1) ||
           (message.read != undefined && message.read == 1)) {
            return;
        }
        message.read = 1;
        var parameters = {
            message_id: message.id
        };
        var config = {
            params: parameters
        };
        $http.get("/message/read-messages", config)
            .then(function(response) {
                if(response) {
                    if(message.chat_id == $scope.chat.id) {
                        $scope.chat.new_messages > 0 ? $scope.chat.new_messages-- : '';
                    }
                    message.read = 1;
                    message.recipient = {read: 1};
                    $scope.unreadMessages.count > 0 ? $scope.unreadMessages.count-- : '';
                } else {
                    //message.read = 0;
                }
            });
    };

    /**
     * Get count unread messages
     */
    $scope.getCountUnreadMessages = function(){
        $http.get("/message/get-count-unread-messages")
            .then(function(response) {
                $scope.unreadMessages.count = response.data;
            });
    };

    /**
     * Get unread messages
     */
    $scope.getUnreadMessages = function(page){
        if(($scope.unreadMessages.messages && $scope.unreadMessages.messages.length > 0 && page == undefined)
            || $scope.unreadMessages.count == 0) {
            return;
        }
        $scope.fetching = true;
        var parameters = {
            page: page
        };
        var config = {
            params: parameters
        };
        $http.get("/message/get-unread-messages", config)
            .then(function(response) {
                if(!$scope.unreadMessages.messages) {
                    $scope.unreadMessages = response.data;
                    console.log($scope.unreadMessages);
                    $scope.unreadMessages.page = 0;
                } else {
                    $scope.unreadMessages.messages.push.apply($scope.unreadMessages.messages, response.data.messages);
                }
                $scope.fetching = false;
            });
    };

    /**
     * Check if message is unread
     */
    $scope.isUnreadMessage = function (message) {
        return message.read != 1 && message.recipient.read != 1;
    };
}]);