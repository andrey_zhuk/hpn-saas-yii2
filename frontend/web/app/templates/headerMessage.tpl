<%if(!_.isEmpty(data)) { %>
    <% _.each(data, function(message) {
        var date_send = moment.unix(message.date_create);
        %>
        <li>
            <a class="dl-horizontal" href="#">
                <div class="pull-left">
                    <img src="http://gravatar.com/avatar/<%=message.profile.gravatar_id %>?s=40" class="img-circle" alt="User Image"/>
                </div>
                <h4>
                    <%=message.user_detail.name%>
                    <small><i class="fa fa-clock-o"></i>&nbsp; <%=jQuery.timeago(date_send ._d)%></small>
                </h4>
                <dt><%=message.message%></dt>
            </a>
        </li>
    <% });
}; %>
