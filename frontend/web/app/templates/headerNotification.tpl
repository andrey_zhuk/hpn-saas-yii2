<%if(!_.isEmpty(data)) { %>
    <% _.each(data, function(message) {
        var date_send = moment.unix(message.date_send);
        %>
        <li>
            <a class="dl-horizontal" href="#">
                <div class="pull-left">
                    <img src="http://gravatar.com/avatar/<%=message.profile.gravatar_id %>?s=40" class="img-circle" alt="User Image"/>
                </div>
                <h4>
                    <%=message.agency.name%>
                    <small><i class="fa fa-clock-o"></i>&nbsp; <%=jQuery.timeago(date_send ._d)%></small>
                </h4>
                <dt style="text-align: left">Update Plan Care</dt>
            </a>
        </li>
    <% });
}; %>
