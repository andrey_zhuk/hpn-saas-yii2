<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
//    public $basePath = '@webroot';
//    public $baseUrl = '@web';

    public $sourcePath = '@webroot/app';
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );

    public $css = [
        'css/dashboard.css',
        'css/skin-blue.min.css',
        'css/jquery.countdown.css',
        'css/site.css',
    ];
    public $js = [
        'js/jquery.countdown.js',
        'js/app.js',
        'js/core.js',
        'js/chat.js',
        'js/notification.js',
        'js/jquery.timeago.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}
