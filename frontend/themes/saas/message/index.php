<?php

$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
$this->title = Yii::$app->name.' - '.Yii::t('app','User Chat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Chat')];

$this->params['pageTitleContent'] = Yii::t('app', 'Chat');

?>
<div class="row user-chat" ng-init="loadHistoryChat(<?= $chat->id ?>)">
    <div class="col-md-8">
        <!-- DIRECT CHAT PRIMARY -->
        <div class="box box-primary direct-chat direct-chat-primary">
            <div ng-show="chat.fetching" class="background-preloader">
                <span class="preloader"></span>
                <!-- image here via CSS -->
            </div>
            <div class="box-header with-border">
                <h3 class="box-title"><?= $member->userData->name ?></h3>
                <div class="box-tools pull-right" ng-show="chat.new_messages > 0">
                    <span data-toggle="tooltip" title="{{ chat.new_messages }} New Messages" class="badge bg-light-blue">
                        {{ chat.new_messages }}
                    </span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="box-body chat list-messages" id="chat-box"
                     scroll-messages="chat.messages" on-scroll="direct-chat-msg" read-msg="readMessage" chat="chat"

                     date-user-name="<?= $model->userData->name ?>" date-profile-img="<?= $model->userData->logo ?>">
                    <a href="" class="btn btn-block btn-default ng-scope"
                       ng-click="loadPreviousMessages(chat)"
                       ng-if="!chat.noMessages"><?= Yii::t('app', 'Show more messages') ?>
                    </a>
                    <!-- Message. Default to the left -->
                    <div class="direct-chat-msg"
                         ng-repeat="message in chat.messages"
                         data-message="{{message}}"
                         ng-mouseover="hoverInMessage();readMessage(message)"
                         ng-mouseleave="hoverOutMessage()"
                         ng-class="{'interlocutor': !checkMineMessage(message), 'unread': isUnreadMessage(message)}">

                        <div class="direct-chat-info clearfix">
                            <a href="/profile/show/{{ chat.members.detail[message.sender.sender].short_id }}">
                                <span class="direct-chat-name pull-left">{{ chat.members.detail[message.sender.sender].name }}</span>
                            </a>
                            <span class="direct-chat-timestamp pull-right">{{ message.date_create }}</span>
                        </div><!-- /.direct-chat-info -->
                        <img class="direct-chat-img" ng-src="{{getUrlPrefix(chat.members.detail[message.sender.sender].avatar_filename)}}" onError="this.onerror=null;this.src='https://s3-us-west-2.amazonaws.com/88creatives-dev/assets/avatar2.jpg';">
                        <div class="direct-chat-text">
                            <span smilies="message.message">{{ message.message }}</span>
                        </div><!-- /.direct-chat-text -->
                        <button type="button" class="btn btn-box-tool update-message"
                                ng-show="buttonDeleteMessage"
                                ng-click="editMessage(message)"
                                ng-if="checkMineMessage(message)">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool remove-message"
                                ng-show="buttonDeleteMessage"
                                ng-click="removeMessage(message)">
                            <i class="fa fa-times"></i>
                        </button>
                    </div><!-- /.direct-chat-msg -->

                </div><!--/.direct-chat-messages-->
                <div id="typing">
                    <li class='text-muted' ng-repeat="user in typing_users">
                        <small>
                            <i class='fa fa-keyboard-o'></i><span> @{{ user.name }} is typing</span>
                        </small>
                    </li>
                </div>
            </div>
            <div class="box-footer">
                <div class="input-group">
                    <input class="form-control" placeholder="Type message..."
                           smilies="chat.message"
                           ng-bind-html="message | smilies"
                           ng-keypress="keyPressOnInputMessage($event);"
                           ng-model="chat.message">

                    <span class="input-group-addon" smilies-selector="chat.message" smilies-placement="top" smilies-title="Smilies"></span>
                    <span class="input-group-btn chat-send2" ng-click="sendMessage()" ng-if="chat.edit_message.id == undefined">
                        <button type="button" class="btn btn-primary btn-flat">Send</button>
                    </span>
                    <span class="input-group-btn chat-send2" ng-click="updateMessage()" ng-if="chat.edit_message.id != undefined">
                        <button type="button" class="btn btn-primary btn-flat">Update</button>
                    </span>
                    <span class="input-group-btn chat-send2" ng-click="cancelEditMessage()" ng-if="chat.edit_message.id != undefined">
                        <button type="button" class="btn btn-default btn-flat">Cancel</button>
                    </span>
                </div>
            </div><!-- /.box-footer-->
        </div><!--/.direct-chat -->
    </div><!-- /.col -->

    <div class="col-md-4">
        <!-- USERS LIST -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Latest Members') ?></h3>
                <div class="box-tools pull-right">
                    <span class="label label-danger"><?= count($latest_members) ?> <?= Yii::t('app', 'New Members') ?></span>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="users-list clearfix">
                    <?php foreach($latest_members as $member): ?>
                        <?php if (!$member->user) continue; ?>
                        <li>
                            <img src="<?= $member->user->userData->getLogo('square') ?>" alt="User Image">
                            <a href="" class="users-list-name"  ng-click="loadHistoryChat(<?= $member->chat_id ?>)">
                                <?= $member->user->userData->name?>
                            </a>
                            <span class="users-list-date"><?= Yii::t('app', 'Today') ?></span>
                        </li>
                    <?php endforeach; ?>
                </ul><!-- /.users-list -->
            </div><!-- /.box-body -->
            <div class="box-footer text-center">
                <a href="javascript:;" class="uppercase"><?= Yii::t('app', 'View All Users') ?></a>
            </div><!-- /.box-footer -->
        </div><!--/.box -->
    </div><!-- /.col -->
</div>
