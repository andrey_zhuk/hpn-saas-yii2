<?php

use yii\widgets\Menu;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img class="settings-detail-ava img-rounded" src="<?= $detail->logo ?>" alt="<?= $detail->name ?>">
            <?= $detail->name ?>
        </h3>
    </div>
    <div class="panel-body">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked'
            ],
            'items' => [
                ['label' => Yii::t('app', 'Profile View'),  'url' => ['/profile/show']],
                ['label' => Yii::t('app', 'Profile Detail'),  'url' => ['/settings/detail']],
                ['label' => Yii::t('app', 'Change password'),  'url' => ['/settings/account']]
            ]
        ]) ?>
    </div>
</div>