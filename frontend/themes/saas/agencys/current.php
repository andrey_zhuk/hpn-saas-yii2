<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/medication.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Current Patients');

$this->params['breadcrumbs'][] = ['label' => $this->title];
$this->params['pageTitleContent'] = Yii::t('app', $this->title);

$url = Url::to(['agencylist']);
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-success">receiving treatment</span>
                    </div>
                </div>
<!--                <div class="box-tools physician-search">-->
<!--                    --><?php
//                        echo Select2::widget([
//                            'model' => $searchModel,
//                            'attribute' => 'name',
//                            'language' => Yii::$app->language,
//                            'options' => ['placeholder' => 'Search ...'],
//                            'theme' => Select2::THEME_BOOTSTRAP,
//                            'hideSearch' => true,
//                            'pluginOptions' => [
//                                'allowClear' => true,
//                                'minimumInputLength' => 1,
//                                'ajax' => [
//                                    'url' => $url,
//                                    'dataType' => 'json',
//                                    'data' => new JsExpression('function(params) { console.log(params); return {q:params.term}; }')
//                                ],
//                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
//                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
//                            ],
//                            'pluginEvents' => [
//                                "change" => "function() { console.log('change'); }",
//                                "select2:opening" => "function() { console.log('select2:opening'); }",
//                                "select2:open" => "function() { console.log('open'); }",
//                                "select2:closing" => "function() { console.log('close'); }",
//                                "select2:close" => "function() { console.log('close'); }",
//                                "select2:selecting" => "function() { console.log('selecting'); }",
//                                "select2:select" => "function(markup) {
//                                    $.pjax({
//                                        type       : 'GET',
//                                        url        : '/physician',
//                                        container  : '#grid-recommendation-agency',
//                                        data       : {'AgencysSearch[name]' : markup.params.data.text},
//                                        push       : true,
//                                        replace    : false,
//                                        timeout    : 10000,
//                                        'scrollTo' : false
//                                    })}",
//                                "select2:unselecting" => "function(par) {
//                                    $.pjax({
//                                        url         : '/physician',
//                                        type        : 'GET',
//                                        container   : '#grid-recommendation-agency',
//                                        timeout     : 2000,
//                                        data       : {'AgencysSearch[id_agency]' : ''},
//                                        push       : true,
//                                        replace    : false,
//                                        timeout    : 10000,
//                                        'scrollTo' : false
//                                    })}",
//                                "select2:unselect" => "function() { console.log('unselect'); }"
//                            ]
//                        ]);
//                    ?>
<!--                </div>-->
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Physician Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Patient Started Care')?></th>
                        <th><?= Yii::t('app', 'Plan Of Care')?></th>
                        <th><?= Yii::t('app', 'Request Medication')?></th>
                    </tr>
                    <?php if (count($patient_care) > 0) : ?>
                        <?php foreach($patient_care as $patient) : ?>
                            <tr>
                                <td><?= Html::a($patient->name, Url::toRoute(['patient/index', 'id' => $patient->id], true)) ?></td>
                                <td><?= Html::a($patient->physician->name, Url::toRoute(['profile/show', 'shortId' => $patient->physician->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($patient->physician->phone)?></td>
                                <td><?= date('m/d/Y', (int)$patient->associateAgency->date_accept); ?></td>
                                <td>
                                    <?= Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/plan-of-care', 'id' => $patient->id], true), [
                                        'title' => Yii::t('app', "Plan Of Care"),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'class' => 'btn btn-sm bg-orange'
                                    ]); ?></td>
                                <td><button class="btn btn-sm btn-info" data-placement="bottom" data-toggle="modal" data-target="#sendRequestMedication" data-patient="<?= $patient->id?>"><?= Yii::t('app', 'Send request') ?></button></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-6">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('app', "Patient Queue Table") ?></h3>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Physician Name')?></th>
                        <th><?= Yii::t('app', 'Date') ?></th>
                        <th><?= Yii::t('app', 'Status') ?></th>
                        <th><?= Yii::t('app', 'Action Items') ?></th>
                    </tr>
                    <?php if (count($patient_queue) > 0) : ?>
                        <?php foreach($patient_queue as $param) : ?>
                            <tr class="patient">
                                <td><?= Html::a($param['patient_name'], Url::toRoute(['patient/index', 'id' => $param['patient_id']], true)) ?></td>
                                <td><?= Html::a($param['physician_name'], Url::toRoute(['profile/show', 'shortId' => $param['physician_short_id']], true)) ?></td>
                                <td><?= date('m/d/Y', $param['date']) ?></td>
                                <td><?= $param['status'] ?></td>
                                <td><?= $param['button'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No referrals patient');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<?php
    echo $this->render('_modal_order', []);
?>
