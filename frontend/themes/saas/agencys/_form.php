<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agencys */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agencys-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agency_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'skilled_nursing')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'physical_therapy')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'speech_language_therapy')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'occupational_therapy')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'home_health_aide')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'medical_social_services')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'summary')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
