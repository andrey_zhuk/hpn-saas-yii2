<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Discharged Patients');

$this->params['breadcrumbs'][] = ['label' => 'Discharged Patients'];
$this->params['pageTitleContent'] = Yii::t('app', 'Discharged Patients');

$url = Url::to(['agencylist']);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-success">treatment completed</span>
                    </div>
                </div>
<!--                <div class="box-tools physician-search">-->
<!--                    --><?php
//                        echo Select2::widget([
//                            'model' => $searchModel,
//                            'attribute' => 'name',
//                            'language' => Yii::$app->language,
//                            'options' => ['placeholder' => 'Search ...'],
//                            'theme' => Select2::THEME_BOOTSTRAP,
//                            'hideSearch' => true,
//                            'pluginOptions' => [
//                                'allowClear' => true,
//                                'minimumInputLength' => 1,
//                                'ajax' => [
//                                    'url' => $url,
//                                    'dataType' => 'json',
//                                    'data' => new JsExpression('function(params) { console.log(params); return {q:params.term}; }')
//                                ],
//                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
//                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
//                            ],
//                            'pluginEvents' => [
//                                "change" => "function() { console.log('change'); }",
//                                "select2:opening" => "function() { console.log('select2:opening'); }",
//                                "select2:open" => "function() { console.log('open'); }",
//                                "select2:closing" => "function() { console.log('close'); }",
//                                "select2:close" => "function() { console.log('close'); }",
//                                "select2:selecting" => "function() { console.log('selecting'); }",
//                                "select2:select" => "function(markup) {
//                                    $.pjax({
//                                        type       : 'GET',
//                                        url        : '/physician',
//                                        container  : '#grid-recommendation-agency',
//                                        data       : {'AgencysSearch[name]' : markup.params.data.text},
//                                        push       : true,
//                                        replace    : false,
//                                        timeout    : 10000,
//                                        'scrollTo' : false
//                                    })}",
//                                "select2:unselecting" => "function(par) {
//                                    $.pjax({
//                                        url         : '/physician',
//                                        type        : 'GET',
//                                        container   : '#grid-recommendation-agency',
//                                        timeout     : 2000,
//                                        data       : {'AgencysSearch[id_agency]' : ''},
//                                        push       : true,
//                                        replace    : false,
//                                        timeout    : 10000,
//                                        'scrollTo' : false
//                                    })}",
//                                "select2:unselect" => "function() { console.log('unselect'); }"
//                            ]
//                        ]);
//                    ?>
<!--                </div>-->
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Physician Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Date Patient Care Ended')?></th>
                    </tr>
                    <?php if (count($patient_care_end) > 0) : ?>
                        <?php foreach($patient_care_end as $patient) : ?>
                            <tr>
                                <td><?= Html::a($patient->name, Url::toRoute(['patient/index', 'id' => $patient->id], true)) ?></td>
                                <td><?= Html::a($patient->physician->name, Url::toRoute(['profile/show', 'shortId' => $patient->physician->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($patient->physician->phone)?></td>
                                <td><?= date('m/d/Y', (int)$patient->associateAgency->date_send); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

