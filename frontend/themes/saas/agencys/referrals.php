<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\MyFormatter;

$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/timer.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'New Patient Referrals');

$this->params['breadcrumbs'][] = ['label' => 'New Patient Referrals'];
$this->params['pageTitleContent'] = Yii::t('app', 'New Patient Referrals');

?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-danger">waiting for approval</span>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Physician Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Date Of Patient Referral')?></th>
                        <th></th>
                    </tr>
                    <?php if (count($patient_coming) > 0) : ?>
                        <?php foreach($patient_coming as $patient) : ?>
                            <tr class="patient">
                                <td><?= Html::a($patient->name, Url::toRoute(['patient/index', 'id' => $patient->id], true)) ?></td>
                                <td><?= Html::a($patient->physician->name, Url::toRoute(['profile/show', 'shortId' => $patient->physician->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($patient->physician->phone)?></td>
                                <td><?= date('m/d/Y', (int)$patient->date_create); ?></td>
                                <td style="white-space:nowrap;" class="text-right">
                                    <?= Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/view-patient-referral-form', 'id' => $patient->id], true), [
                                        'title' => Yii::t('app', "View Patient Referral Form"),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'class' => 'btn btn-sm bg-orange'
                                    ]); ?>
                                    <a class="accept-patient btn btn-sm bg-olive" data-placement="bottom" title="<?= Yii::t('app', 'Accept Patient') ?>" data-toggle="tooltip" href="javascript:;" onclick="App.Api.acceptPatient(<?=$patient->id ?>,this, null);"><i class="fa fa-plus"></i></a>
                                    <?php if ($patient->associateAgency->status_send != \app\models\PatientAssociateAgency::STATUS_REJECT) { ?>
                                        <a class="reject-patient btn btn-sm btn-danger" data-placement="bottom" title="<?= Yii::t('app', 'Reject Patient') ?>" data-toggle="tooltip" href="javascript:;" onclick="App.Api.rejectPatient(<?=$patient->id ?>,this, null);"><i class="fa fa-remove"></i></a>
                                    <?php } else { ?>
                                        <span data-time-reject="<?= $patient->associateAgency->date_end_accept ?>" data-toggle="tooltip" data-placement="bottom"
                                              title="<?= Yii::t('app', ".... is left to take the final decision") ?>" class="timer-accept btn btn-sm btn-danger"></span>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No referrals patient');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>

