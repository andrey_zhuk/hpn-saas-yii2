<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/medication.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Order Request');

$this->params['breadcrumbs'][] = ['label' => $this->title];
$this->params['pageTitleContent'] = Yii::t('app', $this->title);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('app', 'Orders Sent By Physician') ?></h3>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Agency Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Request Text')?></th>
                        <th><?= Yii::t('app', 'Date send')?></th>
                        <th></th>
                    </tr>
                    <?php if (count($order_array) > 0) : ?>
                        <?php foreach($order_array as $order) : ?>
                            <tr>
                                <td><?= Html::a($order['patient_name'], Url::toRoute(['patient/index', 'id' => $order["patient_id"]], true)) ?></td>
                                <td><?= Html::a($order['agency_name'], Url::toRoute(['profile/show', 'id' => $order['agency_id']], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($order['agency_phone'])?></td>
                                <td><?= $order['text'] ?></td>

                                <td><?= $order['date'] ?></td>
                                <td style="width: 1%"><?= $order['button']?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>