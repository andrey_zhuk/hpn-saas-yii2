<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Patients Receiving Care');

$this->params['breadcrumbs'][] = ['label' => 'Patients Receiving Care'];
$this->params['pageTitleContent'] = Yii::t('app', 'Patients Receiving Care');

$url = Url::to(['agencylist']);
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-warning"><?= Yii::t('app', 'waiting for approval')?></span>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Agency Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Treatment start date')?></th>
                        <th><?= Yii::t('app', 'Plan Of Care')?></th>
                        <th><?= Yii::t('app', 'Request Medication')?></th>
                    </tr>
                    <?php if (count($patient_accept) > 0) : ?>
                        <?php foreach($patient_accept as $patient) : ?>
                            <tr>
                                <td><?= Html::a($patient->name, Url::toRoute(['patient/index', 'id' => $patient->id], true)) ?></td>
                                <td><?= Html::a($patient->agency->name, Url::toRoute(['profile/show', 'shortId' => $patient->agency->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($patient->agency->phone)?></td>
                                <td><?= date('m/d/Y', (int)$patient->associateAgency->date_send); ?></td>
                                <td><?= Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/plan-of-care', 'id' => $patient->id], true),[
                                            'title' => Yii::t('app', "Plan of Care"),
                                            'data-toggle' => "tooltip",
                                            'class' => 'btn btn-sm bg-orange'
                                    ]); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('app','Pacient Queue') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">

                    <tr>
                        <th><?= Yii::t('app','Patient Name') ?></th>
                        <th><?= Yii::t('app','Agency Name') ?></th>
                        <th><?= Yii::t('app','Date') ?></th>
                        <th><?= Yii::t('app','Action Items') ?></th>
                    </tr>
                    <?php foreach($patient_array as $param) : ?>
                        <tr>
                            <td><?= Html::a($param['patient_name'], Url::toRoute(['patient/index', 'id' => $param['patient_id']], true)) ?></td>
                            <td><?= Html::a($param['agency_name'], Url::toRoute(['profile/show', 'shortId' => $param['agency_short_id']], true)) ?></td>
                            <td><?= date('m/d/Y', $param['date']) ?></td>
                            <td><?= $param['button'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

