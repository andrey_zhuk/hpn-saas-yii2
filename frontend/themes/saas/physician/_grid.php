<?php
use yii\helpers\Html;
use yii\helpers\Url;

echo kartik\grid\GridView::widget([
    'id' => 'grid-recommendation-agency',
    'dataProvider'=> $dataProvider,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
    'columns' => [
        [
            'attribute' => 'name',
            'content' => function($data){
                return Html::a($data->name, Url::toRoute(['profile/show', 'shortId' => $data->user->short_id], true), [
                    'data-pjax' => '0'
                ]);
            }
        ],
        'admin_name',
        'phone',
        ['label'=>'',
            'format' => 'raw',
            'value'=>function ($data) {
                return Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/new-patient-referral-form', 'shortId' => $data->user->short_id], true), [
                    'title' => Yii::t('app', "Send Patient Referral"),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm bg-orange',
                    'data-toggle' => "tooltip",
                    'data-placement' => "bottom"
                ]);
            },
            'hAlign' => 'center'
        ],
    ],
    'pjaxSettings'=>[
        'neverTimeout'=>false
    ],
    'toolbar' => [
        [
            'content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                    'class' => 'btn btn-default',
                    'title' => Yii::t('app', "Reset Grid")
                ]),
        ],
        '{toggleData}'
    ],
    'toggleDataContainer' => ['class' => 'btn-group-sm'],
    'exportContainer' => ['class' => 'btn-group-sm']
]);