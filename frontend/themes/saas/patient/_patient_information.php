<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MyFormatter;
use common\models\LoginData;

$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/medication.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->

        <div class="box-body profile-card clearfix">
            <div class="profile-picture">
                <?php ($patient->photo) ? $url_avatar = $patient->physician->photo : $url_avatar = Yii::$app->request->baseUrl."/app/images/noavatar.png";  ?>
                <img src="<?= $url_avatar; ?>" width="200">
            </div>
            <div class="profile-overview-content">
                <h2 class="text-left m-left-8"><?= $patient->name?></h2>
                <div class="box-body p-0">
                    <table class="table table-noborder">
                        <?php if ($patient->phone): ?>
                            <tr>
                                <th><?= Yii::t('app', "Phone");?></th>
                                <td>
                                    <?= MyFormatter::phoneFormatter($patient->phone) ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($patient->birthday): ?>
                            <tr>
                                <th><?= Yii::t('app', "Birthday");?></th>
                                <td>
                                    <?= $patient->birthday;  ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($patient->address): ?>
                            <tr>
                                <th><?= Yii::t('app', "Address");?></th>
                                <td>
                                    <?= $patient->address;  ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($patient->email): ?>
                            <tr>
                                <th><?= Yii::t('app', "Email");?></th>
                                <td>
                                    <?= $patient->email;  ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <tr>
                            <th><?= Yii::t('app', "Started Care");?></th>
                            <td>
                                <?php if ($patient->patientAgency) { ?>
                                    <?= date('m/d/Y', (int)$patient->patientAgency->date_accept);  ?>
                                <?php } else { ?>
                                    <span class="label label-warning"><?= Yii::t('app', "The patient is not accepted for treatment yet.") ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div><!-- /.box-header -->
        <?php if (isset($patient->diagnosis->primary_diagnosis)) : ?>
            <blockquote class="callout callout-success">
                <h2 class="page-header">
                    <i class="fa fa-plus-square"></i> <?= Yii::t('app', "Primary Patient diagnosis"); ?>
                    <small class="pull-right"><?= Yii::t('app', 'Date').':';?> <?= $patient->diagnosis->date_primary_diagnosis; ?></small>
                </h2>
                <div class="patient-diagnosis"><?= $patient->diagnosis->primary_diagnosis; ?></div>
            </blockquote>
        <?php endif; ?>

        <?php if (isset($patient->diagnosis->secondary_diagnosis)) : ?>
            <blockquote class="callout callout-warning">
                <h2 class="page-header">
                    <i class="fa fa-plus-square"></i> <?= Yii::t('app', "Secondary Patient diagnosis"); ?>
                    <small class="pull-right"><?= Yii::t('app', 'Date');?>: <?= $patient->diagnosis->date_secondary_diagnosis; ?></small>
                </h2>
                <div class="patient-diagnosis"><?php if (isset($patient->diagnosis->secondary_diagnosis)) echo $patient->diagnosis->secondary_diagnosis; ?></div>
            </blockquote>
        <?php endif; ?>

        <?php if ($this->context->userRole == LoginData::TYPE_PHYSICIAN_NAME) { ?>
            <div class="m-bottom-8">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="page-header">
                            <i class="fa fa-stethoscope"></i> <?= Yii::t('app','Agency')?>&nbsp;-&nbsp;
                            <?= (isset($patient->associateAgencyAccept)) ?
                                Html::a($patient->associateAgencyAccept->name, ['profile/show', 'shortId' => $patient->associateAgencyAccept->user->short_id]) :
                                Yii::t('app', "The agency has not been assigned yet"); ?>
                        </h2>
                    </div><!-- /.col -->
                </div>
                <?php if(isset($patient->associateAgencyAccept)) : ?>
                    <div class="row">
                        <div class="col-md-4 invoice-col">
                            <b><?= Yii::t('app', "Phone") ?>:</b> <?= MyFormatter::phoneFormatter($patient->associateAgencyAccept->phone) ?><br/>
                            <b><?= Yii::t('app', "Phone Fax") ?>:</b> <?= MyFormatter::phoneFormatter($patient->associateAgencyAccept->phone_fax) ?><br/>
                            <b><?= Yii::t('app', "Email") ?>:</b> <?= $patient->associateAgencyAccept->agency_email ?>
                        </div><!-- /.col -->
                        <div class="col-md-4 invoice-col">
                            <b><?= Yii::t('app', "Administrator’s Name") ?>:</b> <?= $patient->associateAgencyAccept->admin_name;?><br/>
                            <b><?= Yii::t('app', "Address") ?>:</b> <?= $patient->associateAgencyAccept->address;?><br/>
                        </div><!-- /.col -->
                    </div>
                    <?php if ($patient->associateAgencyAccept->id != \Yii::$app->user->id) : ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group m-top-12 pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#sendMessage"><?= Yii::t('app', "Send message"); ?></button>
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <?= Html::a(Yii::t('app', "Open chat page"), Url::toRoute(['/message', 'shortId' => $patient->associateAgencyAccept->user->short_id], true)); ?>
                                            </li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:;" data-toggle="modal" data-target="#sendRequestMedication" data-patient="<?= $patient->id?>"><?= Yii::t('app', 'Send request') ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        <?php } elseif ($this->context->userRole == LoginData::TYPE_AGENCY_NAME) { ?>
            <div class="m-bottom-8">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="page-header">
                            <i class="fa fa-stethoscope"></i> <?= Yii::t('app','Physician')?>&nbsp;-&nbsp;<?= Html::a($patient->physician->name, ['profile/show', 'shortId' => $patient->physician->user->short_id]) ?>
                        </h2>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-4 invoice-col">
                        <b><?= Yii::t('app', "Phone") ?>:</b> <?= MyFormatter::phoneFormatter($patient->physician->phone) ?><br/>
                        <b><?= Yii::t('app', "Phone Fax") ?>:</b> <?= MyFormatter::phoneFormatter($patient->physician->phone_fax) ?><br/>
                        <b><?= Yii::t('app', "Email") ?>:</b> <?= $patient->physician->physician_email ?>
                    </div><!-- /.col -->
                    <div class="col-md-4 invoice-col">
                        <b><?= Yii::t('app', "Practice Manager’s Name") ?>:</b> <?= $patient->physician->manager_name;?><br/>
                        <b><?= Yii::t('app', "Address") ?>:</b> <?= $patient->physician->address;?><br/>
                    </div><!-- /.col -->
                </div>
                <?php if ($patient->physician->id != \Yii::$app->user->id) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group m-top-12 pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#sendMessage"><?= Yii::t('app', "Send message"); ?></button>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <?= Html::a(Yii::t('app', "Open chat page"), Url::toRoute(['/message', 'shortId' => $patient->physician->user->short_id], true)); ?>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" data-toggle="modal" data-target="#sendRequestMedication" data-patient="<?= $patient->id?>"><?= Yii::t('app', 'Send request') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php } ?>
    </div>
</div>
