<?php
use yii\bootstrap\Tabs;

$this->title = Yii::$app->name.' - '.Yii::t('app','Referral Forms / Plan of Care');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referral Forms - Plan of Care')];

$this->params['pageTitleContent'] = Yii::t('app', 'Referral Forms - Plan of Care');
$this->params['pageSubTitleContent'] = Yii::t('app', 'New Patient');

?>
<div class="row">
    <div class="col-md-12">
        <div class="box collapsed-box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Patient Referral Form'); ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?=
                $this->render('forms/_referral_form', [
                    'patient' => $patient,
                    'patient_diagnosis' => $patient_diagnosis,
                    'necessary' => $necessary,
                    'face_to_face' => $face_to_face,
                    'physician' => $physician
                ]); ?>
            </div><!-- ./box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box collapsed-box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Plan of Care'); ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?=
                $this->render('forms/_plan_of_care', [
                    'physician' => $physician,
                    'patient' => $patient,
                    'agencys' => $agencys,
                    'plan_care' => $plan_care,
                    'fun_limitations' => $fun_limitations,
                    'activities_permitted' => $activities_permitted,
                    'mental_status' => $mental_status,
                    'prognosis' => $prognosis,
                    'data' => $data
                ]); ?>
            </div><!-- ./box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box collapsed-box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Face-to-Face'); ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    <div class="btn-group">
                        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?=
                $this->render('forms/_face_to_face_form', [
                    'patient' => $patient,
                    'encounter' => $face_to_face,
                    'necessity_list' => $necessity_list,
                    'physician' => $physician,
                    'data' => $data
                ]); ?>
            </div><!-- ./box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
