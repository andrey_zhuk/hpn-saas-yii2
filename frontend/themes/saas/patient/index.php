<?php
use yii\bootstrap\Tabs;
use common\models\LoginData;

$this->title = Yii::$app->name.' - '.Yii::t('app','Patient').' - '.$patient->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Patient')];
$this->params['breadcrumbs'][] = ['label' => $patient->name];

$this->params['pageTitleContent'] = Yii::t('app', 'Patient Details');
$this->params['pageSubTitleContent'] = Yii::t('app', $patient->name);

?>
<script>
    var patient = <?= $patient->id ?>;
</script>
<?php if(Yii::$app->getRequest()->getQueryParam('tab') != null): ?>
    <script>
        $(document).ready(function() {
            $('.nav-tabs a[href="#<?= Yii::$app->getRequest()->getQueryParam('tab') ?>"]').tab('show');
        });
    </script>
<?php endif; ?>
<section class="content nav-tabs-custom">
<?php
    $order_render = ($role == LoginData::TYPE_PHYSICIAN_NAME) ? 'patient_orders_ph' : 'patient_orders';
    /* @var $this PatientController */
    echo Tabs::widget([
        'items' => [
            [
                'label' => 'Patient Information',
                'content' => $this->render('_patient_information', [
                    'patient' => $patient
                ]),
                'active' => true,
                'options' => ['class' => 'fade active in', 'id' => 'patient-information'],
            ],
            [
                'label' => 'Plan of Care',
                'content' => $this->render('patient_plan_of_care_page', [
                    'face_to_face' => $face_to_face,
                    'patient' => $patient,
                    'plan_care' => $plan_care,
                    'agencys' => $agencys,
                    'physician' => $physician,
                    'necessity_list' => $necessity_list,
                    'fun_limitations' => $fun_limitations,
                    'activities_permitted' => $activities_permitted,
                    'mental_status' => $mental_status,
                    'prognosis' => $prognosis,
                    'data' => $data
                ]),
                'options' => ['class' => 'fade', 'id' => 'plan-of-care']
            ],
            [
                'label' => 'Order Requests',
                'content' => $this->render($order_render, [
                    'order_patient' => $order_patient,
                ]),
                'options' => ['class' => 'fade', 'id' => 'patient-orders'],
            ],
        ],
    ]);
?>
</section>
<?= $this->render('/agencys/_modal_order'); ?>

<?php
    if ($role == LoginData::TYPE_PHYSICIAN_NAME && isset($patient->associateAgencyAccept)) {
        $model = $patient->associateAgencyAccept;
        echo $this->render('/profile/_modal_message', [
            'user_data' => $model
        ]);
    }
    elseif ($role == LoginData::TYPE_AGENCY_NAME) {
        $model = $patient->physician;
        echo $this->render('/profile/_modal_message', [
            'user_data' => $model
        ]);
    }
?>
