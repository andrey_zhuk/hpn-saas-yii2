
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Plan of Care'); ?></h3>
                <?php if($plan_care->isNewRecord == true && $this->context->userRole == 'agency') { ?>
                    <span class='label label-warning pull-right'><?= Yii::t('app', "The form hasn't been sent yet") ?></span>
                <?php } else { ?>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                <?php } ?>
            </div>
            <?php if($plan_care->isNewRecord == true && $this->context->userRole == 'agency') { ?>
            <?php } else { ?>
                <div class="box-body">
                    <?=
                    $this->render('forms/_plan_of_care', [
                        'patient' => $patient,
                        'agencys' => $agencys,
                        'physician' => $physician,
                        'plan_care' => $plan_care,
                        'fun_limitations' => $fun_limitations,
                        'activities_permitted' => $activities_permitted,
                        'mental_status' => $mental_status,
                        'prognosis' => $prognosis,
                        'data' => $data
                    ]); ?>

                    <?php if ($data['save_form'] == false) : ?>
                        <div class="row">
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#sendPOCUpdate"><?= Yii::t('app', 'POC Update Request') ?></button>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div><!-- /.col -->
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Face-to-Face'); ?></h3>
                <?php if($face_to_face->isNewRecord == true && $this->context->userRole == 'agency') { ?>
                    <span class='label label-warning pull-right'><?= Yii::t('app', "The form hasn't been sent yet") ?></span>
                <?php } else { ?>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                <?php } ?>
            </div>
            <?php if($face_to_face->isNewRecord == true && $this->context->userRole == 'agency') { ?>
            <?php } else { ?>
                <div class="box-body">
                    <?=
                    $this->render('forms/_face_to_face_form', [
                        'patient' => $patient,
                        'encounter' => $face_to_face,
                        'necessity_list' => $necessity_list,
                        'physician' => $physician,
                        'data' => $data
                    ]); ?>
                </div><!-- ./box-body -->
            <?php } ?>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->

<?php
if($plan_care->isNewRecord == false) {
    echo $this->render('forms/_poc_update', [
        'patient' => $patient
    ]);
}
?>
