<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use kartik\date\DatePicker;
    use dosamigos\ckeditor\CKEditor;
?>
<section class="patient-form clearfix">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header text-center">
                <?= Yii::t('app', "PHYSICIAN FACE-TO-FACE ENCOUNTER FORM"); ?>
            </h2>
        </div><!-- /.col -->
    </div>
    <?php
        $option = [
            'clientOptions' => ['height' => 100],
            'options' => ['rows' => 6],
            'preset' => 'basic'
        ];
        $disabled = ($data['save_face_to_face'] == true) ? '' : 'disabled';
    ?>
    <?php $form = ActiveForm::begin([
        'id' => 'face-to-face-form',
        'enableClientValidation' => $data['save_face_to_face'],
        'enableAjaxValidation' => false,
        'validateOnSubmit' => $data['save_face_to_face'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'offset' => '',
                'wrapper' => 'col-md-12',
            ],
        ],
        'options' => ['enctype' => 'multipart/form-data','class' => 'form-full-width-field']
    ]); ?>
    <!-- info row -->
    <div class="row m-top-20">
        <div class="col-md-9 invoice-col">
            <?= $form->field($patient, 'name', [
                'horizontalCssClasses' => [
                    'label' => '',
                    'wrapper' => 'form-group',
                    'error' => 'disp-none'
                ]
            ])->label(Yii::t('app', "Patient Name") . ':'); ?>
        </div>
        <div class="col-md-3 invoice-col">
            <?= $form->field($patient, 'birthday', [
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'form-group',
                        'error' => 'disp-none'
                    ]
                ])->widget(DatePicker::classname(), [
                    'options' => ['class' => 'datePickerJob', 'id'=>'patient-birthday-face-to-face'],
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'size' => 'md',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'mm/dd/yyyy'
                    ]
                ])->label(Yii::t('app', "Date of Birth") . ':');
            ?>
        </div>
    </div>
    <div class="row m-top-20">
        <p class="col-md-12" style="margin: 0;"><strong><?= Yii::t('app', "Date of Face-to-Face Encounter:") ?>&nbsp;</strong>
            <?= Yii::t('app', "I certify that this patient is under my care and that I, or a Nurse
            Practitioner or Physician Assistant working with me, had a face-to-face encounter with this patient that
            meets the physician face-to-face encounter requirements (please insert date that visit occurred).") ?></p>

        <div class="col-md-2 invoice-col">
            <?= $form->field($encounter, 'date_encounter', [
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'form-group',
                        'error' => 'disp-none'
                    ]
                ])->widget(DatePicker::classname(), [
                    'options' => ['class' => 'datePickerJob'],
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'size' => 'md',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'mm/dd/yyyy'
                    ]
                ])->label(false);
            ?>
        </div>
    </div>
    <div class="row m-top-20">
        <p class="col-md-12"><b><?= Yii::t('app', "Medical Condition:")?> </b><?= Yii::t('app', "The encounter with the patient was in whole, or in part, for the following medical
                condition which is the primary reason for home care. (Please list") ?>&nbsp;<b><?= Yii::t('app', "ALL") ?></b>&nbsp;<?= Yii::t('app', "medical conditions).") ?></p>

        <div class="medical-condition col-md-12">
            <?= $form->field($encounter, 'medical_conditions', [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-md-12',
                ]
            ])->widget(CKEditor::className(), $option)->label(false); ?>
        </div>
    </div>

    <div class="row m-top-20"><p class="col-md-12"><strong><?= Yii::t('app', "Medical Necessity") ?>:</strong>
        <?= Yii::t('app', "I certify, that based on my findings, the following services are medically necessary
        home care services (Please check all that apply).") ?>
        </p>
        <div class="col-md-12">
            <?php echo $form->field($encounter, 'medical_necessity')->inline(true)
                ->checkboxList($necessity_list, [
                'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                    ($checked) ? $val_checked = 'checked' : $val_checked = '';
                    return '
                        <label class="col-sm-6 styleCheck">
                            <input type="checkbox" '.$val_checked.' name="'.$name.'" value="'.$value.'" '.$disabled.' >
                            <span class="mark pull-left"></span>
                            <span class="txt pull-left">'.$label.'</span>
                        </label>
                    ';
                },
            'class' => 'frameSelector levelSelector'])->label(false) ?>
        </div>
    </div>
    <div class="row m-top-20"><p class="col-md-12"><strong><?= Yii::t('app', "Clinical Findings") ?>:</strong>
            <?= Yii::t('app', "My clinical findings support the need for the above services") ?>
            <span class="text-underline"><?= Yii::t('app', "because") ?></span>:
        </p>
        <div class="medical-condition col-md-12">
            <?= $form->field($encounter, 'clinical_findings', [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-md-12',
                ]
            ])->widget(CKEditor::className(), $option)->label(false); ?>
        </div>
    </div>
    <div class="row m-top-20"><p class="col-md-12"><strong><?= Yii::t('app', "Homebound Status") ?>:</strong>
            <?= Yii::t('app', "Further, I certify that my clinical findings support that this patient is homebound because") ?>:
        </p>
        <div class="medical-condition col-md-12">
            <?= $form->field($encounter, 'homebound_status', [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-md-12',
                ]
            ])->widget(CKEditor::className(), $option)->label(false); ?>
        </div>
    </div>
    <div class="row m-top-30">
        <div class="col-md-9 invoice-col">
            <?php echo $form->field($physician,'name',
                ['horizontalCssClasses' => [
                'label' => '',
                'wrapper' => 'form-group',
                'error' => 'disp-none'
            ]])->label(Yii::t('app', "Physician Signature") . ':');; ?>
        </div>

        <div class="col-md-3 invoice-col">
            <label><?php echo Yii::t('app', "Date: ");?></label>
            <?= $form->field($encounter, 'date_of_drawing', [
                'horizontalCssClasses' => [
                    'label' => '',
                    'wrapper' => 'form-group',
                    'error' => 'disp-none'
                ]
            ])->widget(DatePicker::classname(), [
                'options' => ['class' => 'datePickerJob'],
                'type' => DatePicker::TYPE_INPUT,
                'language' => Yii::$app->language,
                'size' => 'md',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'mm/dd/yyyy'
                ]
            ])->label(false); ?>
        </div>
        <div class="col-md-12 invoice-col">
            <?= $form->field($physician, 'name', [
                'horizontalCssClasses' => [
                    'label' => '',
                    'wrapper' => 'form-group',
                    'error' => 'disp-none'
                ],
                'inputOptions'=>[
                    'disabled'=>'disabled',
                ]
            ])->label(Yii::t('app', "Physician Printed Name") . ':'); ?>
        </div>
    </div>
    <?php if($data['save_face_to_face'] == true) : ?>
        <?= Html::submitButton(Yii::t('app', 'Save & Send'), ['class' => 'btn btn-primary pull-right m-top-30']) ?><br>
    <?php endif; ?>

    <?php ActiveForm::end(); ?>
    <div class="section-fixed section-loading">
        <div class="loading-icon"><img src="/app/images/loading.gif" alt=""/></div>
    </div>
</section>

<script>
    $(window).load(function() {
        $('.section-loading').hide();
    });
</script>