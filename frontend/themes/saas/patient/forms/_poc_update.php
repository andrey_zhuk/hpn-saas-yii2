<?php
use dosamigos\ckeditor\CKEditor;

$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/poc.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="modal fade modal-warning" id="sendPOCUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= Yii::t('app', "PLAN OF CARE") ?>&nbsp;-&nbsp;<?= Yii::t('app', "Update Request") ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissable disp-none">
                    <div class="error-message"><ul></ul></div>
                </div>
                <div class="p-2-5">
                    <label for="modal-plancare-medications" class="control-label">10. <?= Yii::t('app', "Medications: Dose/Frequency/Route (N)ew (C)hanged") ?></label>
                    <?= CKEditor::widget([
                            'clientOptions' => ['height' => 100],
                            'options' => ['class' => 'message-text'],
                            'preset' => 'basic',
                            'name' => 'modal-plancare-medications',
                            'id' => 'modal-plancare-medications'
                        ]
                    );?>
<!--                    <textarea cols="5" rows="4" class="form-control" id="modal-plancare-medications"></textarea>-->
                </div>
                <div class="p-2-5">
                    <label for="modal-plancare-dme_supplies" class="control-label">14. <?= Yii::t('app', "DME and Supplies") ?></label>
                    <input type="text" class="form-control" id="modal-plancare-dme_supplies">
                </div>
                <div class="p-2-5">
                    <label for="modal-plancare-treatments" class="control-label">21. <?= Yii::t('app', "Orders for Discipline and Treatments (Specify Amount/Frequency/Duration)") ?></label>
<!--                    <textarea class="form-control" id="modal-plancare-treatments"></textarea>-->
                    <?= CKEditor::widget([
                            'clientOptions' => ['height' => 100],
                            'options' => ['class' => 'message-text'],
                            'preset' => 'basic',
                            'name' => 'modal-plancare-treatments',
                            'id' => 'modal-plancare-treatments'
                        ]
                    );?>
                </div>
                <div class="p-2-5">
                    <label for="modal-plancare-discharge_plans" class="control-label">22. <?= Yii::t('app', "Goals/Rehabilitation Potential/Discharge Plans") ?></label>
<!--                    <textarea rows="3" class="form-control" id="modal-plancare-discharge_plans"></textarea>-->
                    <?= CKEditor::widget([
                            'clientOptions' => ['height' => 100],
                            'options' => ['class' => 'message-text'],
                            'preset' => 'basic',
                            'name' => 'modal-plancare-discharge_plans',
                            'id' => 'modal-plancare-discharge_plans'
                        ]
                    );?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><?= Yii::t('app', "Close") ?></button>
                <button type="button" class="btn btn-default request-update-poc" data-patient="<?= $patient->id ?>"><?= Yii::t('app', "Send request") ?></button>
            </div>
            <div class="section-fixed section-loading disp-none">
                <div class="loading-icon"><img src="/app/images/loading.gif" alt=""/></div>
            </div>
        </div>
    </div>
</div>
