<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use kartik\date\DatePicker;
    use dosamigos\ckeditor\CKEditor;
?>


<!-- Main content -->
<section class="patient-form clearfix">
    <div class="row">
        <div class="col-md-6">
            <?= Yii::t('app', "Department of Health and Human Services") ?><br>
            <?= Yii::t('app', "Centers for Medicare & Medicaid Services") ?>
        </div><!-- /.col -->
        <div class="col-md-6 text-right">
            <?= Yii::t('app', "Form Approved") ?><br>
            <?= Yii::t('app', "OMB No. 0938-0357") ?>
        </div><!-- /.col -->
    </div>
    <hr class="hrborder-2 m-bottom-0">
    <!-- title row -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">
                <?= Yii::t('app', "HOME HEALTH CERTIFICATION AND PLAN OF CARE") ?>
            </h3>
        </div><!-- /.col -->
    </div>
    <?php
        $option = array('rows' => 5,
            'options' => array('plugins' => array('clips', 'fontfamily'), 'lang' => 'sv')
        );
        $disabled = ($data['save_form'] == true) ? '' : 'disabled';
    ?>

    <?php $form = ActiveForm::begin([
        'id' => 'plan-of-care',
        'enableClientValidation' => $data['save_form'],
        'enableAjaxValidation' => false,
        'validateOnSubmit' => $data['save_form'],
        'options' => ['enctype' => 'multipart/form-data','class' => 'form-full-width-field']
    ]); ?>
    <div class="table-plan-care">
        <div class="table-row border-top clearfix">
            <div class="table-cell-20 p-2-5">
                <?= $form->field($plan_care, 'claim', [
                    ])->label('1. '.Yii::t('app', "Patient's HI Claim No."));
                ?>
            </div>
            <div class="table-cell-15 p-2-5">
                <?= $form->field($plan_care, 'start_care_date', [
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'form-group',
                        'error' => 'disp-none'
                    ],
                    'inputOptions' => ['class'=>'datePickerJob']
                ])->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'size' => 'md',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'mm/dd/yyyy'
                    ]
                ])->label('2. '.Yii::t('app', "Start Of Care Date"));
                ?>
            </div>
            <div class="table-cell-30 p-2-5">
                <label>3. <?= Yii::t('app', "Certification Period"); ?></label>
                <div class="cert-period"><?= Yii::t('app', "From") ?>:&nbsp;
                    <?= $form->field($plan_care, 'certification_period_start',
                        ['enableError' => false, 'inputOptions' => ['class'=>'datePickerJob']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['class' => 'datePickerJob'],
                        'type' => DatePicker::TYPE_INPUT,
                        'language' => Yii::$app->language,
                        'size' => 'md',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'mm/dd/yyyy'
                        ]
                    ])->label(false);
                    ?>
                <?= Yii::t('app', "To")?>:&nbsp;
                    <?= $form->field($plan_care, 'certification_period_finish',
                        ['enableError' => false])
                    ->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_INPUT,
                        'language' => Yii::$app->language,
                        'size' => 'md',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'mm/dd/yyyy'
                        ]
                    ])->label(false); ?>
                </div>
            </div>
            <div class="table-cell-20 p-2-5">
                <?= $form->field($plan_care, 'medical_record', [
                    ])->label('4. '.Yii::t('app', "Medical Record No."));
                ?>
            </div>
            <div class="table-cell-15 p-2-5">
                <?= $form->field($plan_care, 'provider_no', [
                    ])->label('5. '.Yii::t('app', "Provider No."));
                ?>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-30 p-2-5">
                <?= $form->field($patient, 'name', [
                    ])->label('6. '.Yii::t('app', "Patient's Name"));
                ?>
            </div>
            <div class="table-cell-30 p-2-5">
                <?= $form->field($patient, 'address', [
                    ])->label(Yii::t('app', "Patient's Address"));
                ?>
            </div>
            <div class="table-cell-40 p-2-5">
                <label>7. <?= Yii::t('app', "Provider's Name, Address and Telephone Number");?></label><br>
                <?php if (isset($agencys)) : ?>
                    <span class="text-b-i"><?= $agencys->name?></span>,&nbsp<?= $agencys->address ?><br>
                    <b>tel.: &nbsp<?= $agencys->phone ?></b>
                <?php else : ?>
                    <?= Yii::t('app', "no associate agency") ?>
                <?php endif ?>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-0">
                <div class="table-row table-none clearfix">
                    <div class="table-cell-60 p-2-5">
                        <?= $form->field($patient, 'birthday', [
                            'horizontalCssClasses' => [
                                'label' => '',
                                'wrapper' => 'form-group',
                                'error' => 'disp-none'
                            ],
                        ])->widget(DatePicker::classname(), [
                            'options' => ['id'=>'patient-birthday-plan-of-care'],
                            'type' => DatePicker::TYPE_INPUT,
                            'language' => Yii::$app->language,
                            'size' => 'md',
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'mm/dd/yyyy'
                            ]
                        ])->label('8. '.Yii::t('app', "Date of Birth"));
                        ?>
                    </div>
                    <div class="table-cell-40 p-2-5">
                        <?= $form->field($patient, 'gender')->inline()->radioList(
                            $data['gender'],
                            [
                                'class' => 'btn-group',
                                'data-toggle' => 'buttons',
                                'unselect' => null, // remove hidden field],
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return '<label class="btn btn-default' . ($checked ? ' active' : '') . '">' .
                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                },
                            ]
                        )->label('9. '.Yii::t('app', "Sex")) ?>
                    </div>
                </div>
                <div class="table-row border-top clearfix">
                    <div class="table-cell-20 p-2-5">
                        <?= $form->field($plan_care, 'principal_icd', [
                            ])->label('11. '.Yii::t('app', "ICD-9-CM"));
                        ?>
                    </div>
                    <div class="table-cell-60 p-2-5">
                        <?= $form->field($plan_care, 'principal', [
                            ])->label(Yii::t('app', "Principal Diagnosis"));
                        ?>
                    </div>
                    <div class="table-cell-20 p-2-5">
                        <?= $form->field($plan_care, 'principal_date',
                            ['enableError' => false])
                        ->widget(DatePicker::classname(), [
                            'type' => DatePicker::TYPE_INPUT,
                            'language' => Yii::$app->language,
                            'size' => 'md',
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'mm/dd/yyyy'
                            ]
                        ])->label(Yii::t('app', "Date")); ?>
                    </div>
                </div>
                <div class="table-row border-top clearfix">
                    <div class="table-cell-20 p-2-5">
                        <?= $form->field($plan_care, 'procedure_icd', [
                            ])->label('12. '.Yii::t('app', "ICD-9-CM"));
                        ?>
                    </div>
                    <div class="table-cell-60 p-2-5">
                        <?= $form->field($plan_care, 'procedure', [
                        ])->label(Yii::t('app', "Surgical Procedure"));
                        ?>
                    </div>
                    <div class="table-cell-20 p-2-5">
                        <?= $form->field($plan_care, 'procedure_date',
                            ['enableError' => false])
                            ->widget(DatePicker::classname(), [
                                'type' => DatePicker::TYPE_INPUT,
                                'language' => Yii::$app->language,
                                'size' => 'md',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'mm/dd/yyyy'
                                ]
                            ])->label(Yii::t('app', "Date")); ?>
                    </div>
                </div>
                <div class="table-row border-top clearfix">
                    <div class="table-cell-20 p-2-5">
                        <?= $form->field($plan_care, 'other_diagnoses_icd', [
                            ])->label('13. '.Yii::t('app', "ICD-9-CM"));
                        ?>
                    </div>
                    <div class="table-cell-60 p-2-5">
                        <?= $form->field($plan_care, 'other_pertinent_diagnoses', [
                            ])->label(Yii::t('app', "Other Pertinent Diagnoses"));
                        ?>
                    </div>
                    <div class="table-cell-20 p-2-5">
                        <?= $form->field($plan_care, 'other_diagnoses_date',
                            ['enableError' => false])
                            ->widget(DatePicker::classname(), [
                                'type' => DatePicker::TYPE_INPUT,
                                'language' => Yii::$app->language,
                                'size' => 'md',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'mm/dd/yyyy'
                                ]
                        ])->label(Yii::t('app', "Date")); ?>
                    </div>
                </div>
            </div>
            <div class="table-cell-50 p-2-5">
                <?= $form->field($plan_care, 'medications')->textarea(array('rows'=>8,'cols'=>5))
                    ->label('10. '.Yii::t('app', "Medications: Dose/Frequency/Route (N)ew (C)hanged")); ?>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-2-5">
                <?= $form->field($plan_care, 'dme_supplies', [
                    ])->label('14. '.Yii::t('app', "DME and Supplies")); ?>
            </div>
            <div class="table-cell-50 p-2-5">
                <?= $form->field($plan_care, 'safety_measures', [
                    ])->label('15. '.Yii::t('app', "Safety Measures")); ?>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-2-5">
                <?= $form->field($plan_care, 'nutritional_req', [
                    ])->label('16. '.Yii::t('app', "Nutritional Req.")); ?>
            </div>
            <div class="table-cell-50 p-2-5">
                <?= $form->field($plan_care, 'allergies', [
                    ])->label('17. '.Yii::t('app', "Allergies:")); ?>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-2-5">
                <label>18.A. Functional Limitations</label>
                <table class="allergies" style="width: 100%">
                    <?= $form->field($plan_care, 'functional_limitations',
                        ['options' => ['class' => 'clearfix']])->inline(true)
                        ->checkboxList($fun_limitations, [
                            'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                                ($checked) ? $val_checked = 'checked' : $val_checked = '';

                                $html = '<td><span class="txt pull-left p-right-10">'.$label->num.'</span></td>
                                    <td><label class="styleCheck">
                                    <input type="checkbox" '.$val_checked.' name="'.$name.'" value="'.$value.'" '.$disabled.'>
                                    <span class="mark pull-left"></span></label></td>
                                    <td><span style="padding: 0 10px 0 5px;" class="txt pull-left ">'.$label->name.'</span>
                                </td>';
                                if (($index % 2) != 0) $html = $html.'</tr>';
                                else $html = '<tr>'.$html;

                                return $html;
                            },
                            ])->label(false) ?>
                </table>
                <?= $form->field($plan_care, 'functional_limitations_other', [
                    ])->label(false); ?>
            </div>
            <div class="table-cell-50 p-2-5">
                <label>18.B. <?= Yii::t('app', "Activities Permitted") ?></label>
                <table class="allergies" style="width: 100%">
                <?= $form->field($plan_care, 'activities_permitted',
                    ['options' => ['class' => 'clearfix']])->inline(true)
                    ->checkboxList($activities_permitted, [
                        'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                            ($checked) ? $val_checked = 'checked' : $val_checked = '';
                            $html = '<td><span class="txt pull-left p-right-10">'.$label->num.'</span></td>
                                    <td><label class="styleCheck">
                                    <input type="checkbox" '.$val_checked.' name="'.$name.'" value="'.$value.'" '.$disabled.'>
                                    <span class="mark pull-left"></span></label></td>
                                    <td><span style="padding: 0 10px 0 5px;" class="txt pull-left ">'.$label->name.'</span>
                                </td>';
                            if (($index % 2) != 0) $html = $html.'</tr>';
                            else $html = '<tr>'.$html;

                            return $html;
                        },
                    ])->label(false) ?>
                </table>
                <?= $form->field($plan_care, 'activities_permitted_other', [
                ])->label(false); ?>
            </div>
        </div>
        <div class="table-row border-top p-2-5 clearfix">
            <?= $form->field($plan_care, 'mental_status')->inline(true)
                ->checkboxList($mental_status, [
                    'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                        ($checked) ? $val_checked = 'checked' : $val_checked = '';
                        return '<label class="styleCheck col-md-3">
                            <span class="txt p-right-10 pull-left">'.$label->num.'</span>
                            <input type="checkbox" '.$val_checked.' name="'.$name.'" value="'.$value.'" '.$disabled.'>
                            <span class="mark pull-left"></span>
                            <span class="txt pull-left" style="padding: 0 10px 0 5px;">'.$label->name.'</span>
                        </label>';
            }])->label('19. '.Yii::t('app', "Mental Status:")); ?>
        </div>
        <div class="table-row border-top p-2-5 clearfix">
            <?= $form->field($plan_care, 'prognosis')->radioList($prognosis, [
            'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                ($checked) ? $val_checked = 'checked' : $val_checked = '';
                return '<label class="styleCheck col-md-3">
                            <input type="radio" '.$val_checked.' name="'.$name.'" value="'.$value.'" '.$disabled.'>
                            <span class="mark pull-left"></span>
                            <span class="txt pull-left" style="padding: 0 10px 0 5px;">'.$label.'</span>
                        </label>';
            }])->label('20. '.Yii::t('app', "Prognosis:")); ?>
        </div>
        <div class="table-row border-top p-2-5 clearfix">
            <?= $form->field($plan_care, 'treatments')->textarea(array('rows'=>8))
                ->label('21. '.Yii::t('app', "Orders for Discipline and Treatments (Specify Amount/Frequency/Duration)")); ?>
        </div>
        <div class="table-row border-top p-2-5 clearfix">
            <?= $form->field($plan_care, 'discharge_plans')->textarea(array('rows'=>3))
                ->label('22. '.Yii::t('app', "Goals/Rehabilitation Potential/Discharge Plans")); ?>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-2-5">
                <label style="margin-bottom: 40px;">23. <?= Yii::t('app', "Nurse's Signature and Date of Verbal SOC Where Applicable") ?>:</label>
            </div>
            <div class="table-cell-50 p-2-5">
                <label style="margin-bottom: 40px;">25. <?= Yii::t('app', "Date HHA Received Signed POT") ?></label>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-2-5">
                <label>24. <?= Yii::t('app', "Physician's Name and Address") ?></label><br>
                <?= $physician->name; ?>,&nbsp;<?= $physician->address; ?>
            </div>
            <div class="table-cell-50 p-2-5">
                <label>26.  <?= Yii::t('app', "I certify/recertify that this patient is confined to his/her home and needs intermittent skilled nursing care, physical therapy and/or speech therapy or continues to need occupational therapy. The patient is under my care, and I have authorized the services on this plan of care and will periodically review the plan") ?>.</label>
            </div>
        </div>
        <div class="table-row border-top clearfix">
            <div class="table-cell-50 p-2-5">
                <label>27. <?= Yii::t('app', "Attending Physician's Signature and Date Signed") ?></label>
            </div>
            <div class="table-cell-50 p-2-5">
                <label>28.  <?= Yii::t('app', "Anyone who misrepresents, falsifies, or conceals essential information required for payment of Federal funds may be subject to fine, imprisonment, or civil penalty under applicable Federal laws") ?>.</label>
            </div>
        </div>
    </div>

    Form CMS-485 (C-3) (02-94) (Formerly HCFA-485) (Print Aligned)

    <div>
        <h2 class="text-center">Privacy Act Statement</h2>
        Sections 1812, 1814, 1815, 1816, 1861, and 1862 of the Social Security Act authorize collection of this information. The primary use of this information is to process and pay Medicare benefits to or on behalf of eligible individuals. Disclosure of this information may be made to : Peer Review Organizations and Quality Review Organizations in connection with their review of claims, or in connection with studies or other review activities, conducted pursuant to Part B of Title XI of the Social Security Act; State Licensing Boards for review of unethical practices or nonprofessional conduct; A congressional office from the record of an individual in response to an inquiry from the congressional office at the request of that individual.
        Where the individual's identification number is his/her Social Security Number (SSN), collection of this information is authorized by Executive Order 9397. Furnishing the information on this form, including the SSN, is voluntary, but failure to do so may result in disapproval of the request for payment of Medicare benefits.
        <h2 class="text-center">Paper Work Burden Statement</h2>
        According to the Paperwork Reduction Act of 1995, no persons are required to respond to a collection of information unless it displays a valid OMB control number. The valid OMB control number for this information collection is 0938-0357. The time required to complete this information collection is estimated to aver-age 15 minutes per response, including the time to review instructions, search existing data resources, gather the data needed, and complete and review the information collection. If you have any comments concerning the accuracy of the time estimate(s) or suggestions for improving this form, please write to: CMS, Mailstop N2-14-26, 7500 Security Boulevard, Baltimore, Maryland 21244-1850.
    </div>
    <?php if ($data['save_form'] == true) : ?>
        <?= Html::submitButton(($plan_care->isNewRecord) ? Yii::t('app', 'Save & Send') : Yii::t('app', 'Update'), ['class'=> 'btn btn-primary pull-right']) ;?>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</section><!-- /.content -->