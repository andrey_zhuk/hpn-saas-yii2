<?php
use yii\bootstrap\Tabs;
$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/timer.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::$app->name.' - '.Yii::t('app','Referral Forms / Plan of Care');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referral Forms - Plan of Care')];

$this->params['pageTitleContent'] = Yii::t('app', 'Referral Forms - Plan of Care');
$this->params['pageSubTitleContent'] = $patient->name;

?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right" style="margin-bottom: 10px;">
            <a class="accept-patient btn btn-sm bg-olive" data-placement="bottom" title="<?= Yii::t('app', 'Accept Patient') ?>" data-toggle="tooltip" href="javascript:;" onclick="App.Api.acceptPatient(<?=$patient->id ?>,this,'ref');"><?= Yii::t('app', 'Accept') ?></a>
            <?php if ($patient->associateAgency->status_send != \app\models\PatientAssociateAgency::STATUS_REJECT) { ?>
                <a class="reject-patient btn btn-sm btn-danger" data-placement="bottom" title="<?= Yii::t('app', 'Reject Patient') ?>" data-toggle="tooltip" href="javascript:;" onclick="App.Api.rejectPatient(<?=$patient->id ?>,this,'ref');"><?= Yii::t('app', 'Reject') ?></a>
            <?php } else { ?>
                <span data-time-reject="<?= $patient->associateAgency->date_end_accept ?>" data-toggle="tooltip" data-placement="bottom"
                      title="<?= Yii::t('app', ".... is left to take the final decision") ?>" class="timer-accept btn btn-sm btn-danger"></span>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-warning <?= ($patient_diagnosis == NULL) ? 'collapsed-box' : ''?>">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Patient Referral Form'); ?></h3>
                <?= ($patient_diagnosis == NULL) ? "<span class='label label-warning pull-right'>The form hasn't been sent yet</span>" : '' ?>
                <?php if ($patient_diagnosis != NULL) : ?>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                <?php endif; ?>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php
                    if ($patient_diagnosis != NULL) {
                        echo $this->render('forms/_view_referral_form', [
                            'patient' => $patient,
                            'patient_diagnosis' => $patient_diagnosis,
                            'necessary' => $necessary,
                            'face_to_face' => $face_to_face,
                            'physician' => $physician
                        ]);
                    }
                ?>
            </div><!-- ./box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger <?= ($plan_care == NULL) ? 'collapsed-box' : ''?>">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Plan of Care'); ?></h3>
                <?= ($plan_care == NULL) ? "<span class='label label-warning pull-right'>The form hasn't been sent yet</span>" : '' ?>
                <?php if ($plan_care != NULL) : ?>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                <?php endif; ?>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php
                    if ($plan_care != NULL) {
                        echo $this->render('forms/_view_plan_of_care', [
                            'physician' => $physician,
                            'patient' => $patient,
                            'agencys' => $agencys,
                            'plan_care' => $plan_care,
                            'fun_limitations' => $fun_limitations,
                            'activities_permitted' => $activities_permitted,
                            'mental_status' => $mental_status,
                            'prognosis' => $prognosis,
                            'data' => $data
                        ]);
                    }
                ?>
            </div><!-- ./box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box box-info <?= ($face_to_face == NULL) ? 'collapsed-box' : ''?>">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Face-to-Face'); ?></h3>
                <?= ($face_to_face == NULL) ? "<span class='label label-warning pull-right'>The form hasn't been sent yet</span>" : '' ?>
                <?php if ($face_to_face != NULL) : ?>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                <?php endif; ?>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php
                    if ($face_to_face != NULL) {
                        echo $this->render('forms/_view_face_to_face_form', [
                            'patient' => $patient,
                            'encounter' => $face_to_face,
                            'necessity_list' => $necessity_list,
                            'physician' => $physician
                        ]);
                    }
                ?>
            </div><!-- ./box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
