<?php
use yii\jui\AutoComplete;
use kartik\grid\GridView;

/* @var $this PhysicianController */
//echo AutoComplete::widget([
//    'model' => $agency,
//    'attribute' => 'email',
//    'clientOptions' => [
//        'source' => ['USA', 'RUS'],
//    ],
//]);
echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
        'beforeGrid'=>'My fancy content before.',
        'afterGrid'=>'My fancy content after.',
    ]
]);
//$this->pageTitle = Yii::app()->name . ' - Physician Landing page';
//$this->pageTitleContent = 'Physician';
//$this->pageSubTitleContent = Yii::t('zii','Landing page');
//$this->breadcrumbs=array(
//    'Physician',
//);
//
//Yii::app()->clientScript->registerScript('search', "
//$('#physician-search-form').submit(function(){
//    searchPhisician();
//    return false;
//});
//function searchPhisician() {
//    $.fn.yiiJsonGridView.update('phisician-grid', {
//        data: 'PhysicianRecomendationAgency[name]='+$('#agency_name_search').val()
//    });
//}
//");
?>

<div class="row">
    <div class="col-xs-6">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">Recommendation Home Halth Agencies</h3>
                <div class="box-tools">
                    <form id="physician-search-form">
                        <div class="input-group">
                            <?php
//                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
//                                'model'=>$agency,
//                                'attribute'=>'name',
//                                'source'=>$this->createUrl('physician/search'),
//                                'options'=>array(
//                                    'minLength'=>'2',
//                                    'showAnim'=>'fold',
//                                    'select'=>'js:function( event, ui ) {
//                                        this.value = ui.item.label;
//                                        searchPhisician();
//                                    }'
//                                ),
//                                'htmlOptions' => array(
//                                    'maxlength'=>50,
//                                    'placeholder'=>"Search",
//                                    'name'=>'name_search',
//                                    'class'=>"form-control input-sm pull-right",
//                                    'style'=>"width: 150px;"
//                                ),
//                            ));
                            ?>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-default search-phisician"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive p-0">
                <?php Pjax::begin(['id' => 'countries']) ?>
                <?php
//                    $this->widget(
//                        'booster.widgets.TbJsonGridView',
//                        array(
//                            'dataProvider' => $agency->search_agencys(),
//                            'id'=>'phisician-grid',
//                            'type' => 'hover',
//                            'summaryText' => false,
//                            'cssFile' => '/css/grid-table.css',
//                            'cacheTTL' => 10, // cache will be stored 10 seconds (see cacheTTLType)
//                            'cacheTTLType' => 's', // type can be of seconds, minutes or hours
//                            'columns' => array(
//                                array( 'name'=>'name',
//                                    'type'=>'raw',
//                                    'value' => 'CHtml::link(CHtml::encode($data->agencys->name),array("user/user/view","id"=>$data->id_agency))'),
//                                array( 'name'=>'agency_admin_name', 'value'=>'$data->agencys ? $data->agencys->admin_name: "-"'),
//                                array( 'name'=>'agency_phone', 'value'=>'$data->agencys ? $data->agencys->phone: "-"'),
//                                array(
//                                    'class' => 'booster.widgets.TbJsonButtonColumn',
//                                    'template' => '{postview}',
//                                    'buttons' => array(
//                                        'postview' => array(
//                                            'icon'=>'fa fa-paper-plane',
//                                            'label' => 'Send Patient Referral',     // text label of the button
//                                            'url' => '',       // the PHP expression for generating the URL of the button
////                                            'options' => array(...), // HTML options for the button tag
//                                            'click' => 'function(){console.log("sdfsdf"); return false;}'     // a JS function to be invoked when the button is clicked
//                                        ),
//                                    )
//                                ),
//                            ),
//                        )
//                    );
                ?>
                <?php Pjax::end() ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Pacient Queue</h3>
                <div class="box-tools">
                    <div class="input-group">
                        <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                        <div class="input-group-btn">
                            <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Reason</th>
                    </tr>
                    <tr>
                        <td>183</td>
                        <td>John Doe</td>
                        <td>11-7-2014</td>
                        <td><span class="label label-success">Approved</span></td>
                        <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                    </tr>
                    <tr>
                        <td>219</td>
                        <td>Alexander Pierce</td>
                        <td>11-7-2014</td>
                        <td><span class="label label-warning">Pending</span></td>
                        <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                    </tr>
                    <tr>
                        <td>657</td>
                        <td>Bob Doe</td>
                        <td>11-7-2014</td>
                        <td><span class="label label-primary">Approved</span></td>
                        <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                    </tr>
                    <tr>
                        <td>175</td>
                        <td>Mike Doe</td>
                        <td>11-7-2014</td>
                        <td><span class="label label-danger">Denied</span></td>
                        <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                    </tr>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>
<?php
//    foreach ($agency as $key => $val) {
//        echo $val;
//    }
//    die();
?>
<?php
//var_dump('<pre>',$agency,'</pre>');
//$this->widget(
//    'booster.widgets.TbGridView',
//    array(
//        'dataProvider' => $agency,
//        'template' => "{items}",
////        'columns' => $agency,
//    )
//);

?>