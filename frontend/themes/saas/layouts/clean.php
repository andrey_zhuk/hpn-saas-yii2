<?php $this->beginContent('@app/themes/saas/layouts/main.php'); ?>
<?php
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;
use common\models\LoginData;

?>

<section class="section_internal">
    <div class="container">
        <?php echo $content; ?>
    </div>
</section>

<?php $this->endContent();?>
