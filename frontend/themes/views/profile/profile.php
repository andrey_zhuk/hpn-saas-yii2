<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->pageTitleContent = UserModule::t("Profile");
$this->pageSubTitleContent = Yii::t('zii','View');
$this->breadcrumbs=array(
	UserModule::t("Profile"),
);
?>

<?php
$this->menu=array(
	((UserModule::isAdmin()) ? array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')) : array()),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
    array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
    array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword')),
);
?>
<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>
<?php /** @var TbActiveForm $form */
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'horizontalForm',
        'type' => 'horizontal',
    )
); ?>

<?php
echo $form->textFieldGroup($model, 'username');
//echo $form->passwordFieldGroup($model, 'password');
//echo $form->checkboxGroup($model, 'checkbox');
//echo $form->textFieldGroup(
//    $model->getAttributeLabel('username'),
//    'username',
//    array(
//        'wrapperHtmlOptions' => array(
//            'class' => 'col-sm-5',
//        ),
//        'hint' => 'In addition to freeform text, any HTML5 text-based input appears like so.'
//    )
//);
?>
<?php
$this->endWidget();
unset($form);
?>
<table class="dataGrid">
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('username')); ?></th>
	    <td><?php echo CHtml::encode($model->username); ?></td>
	</tr>
	<?php 
		$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
		if ($profileFields) {
			foreach($profileFields as $field) {
				//echo "<pre>"; print_r($profile); die();
			?>
	<tr>
		<th class="label"><?php echo CHtml::encode(UserModule::t($field->title)); ?></th>
    	<td><?php echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?></td>
	</tr>
			<?php
			}//$profile->getAttribute($field->varname)
		}
	?>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
    	<td><?php echo CHtml::encode($model->email); ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('create_at')); ?></th>
    	<td><?php echo $model->create_at; ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('lastvisit_at')); ?></th>
    	<td><?php echo $model->lastvisit_at; ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></th>
    	<td><?php echo CHtml::encode(User::itemAlias("UserStatus",$model->status)); ?></td>
	</tr>
</table>
