<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("My Profile");

$this->pageTitleContent = UserModule::t("My Profile");
$this->pageSubTitleContent = Yii::t('zii','Edit');

$this->breadcrumbs=array(
	UserModule::t("Profile")=>array('profile'),
	UserModule::t("Edit"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'))
		:array()),
    ((UserModule::isAdmin())
        ?array('label'=>UserModule::t('List User'), 'url'=>array('/user'))
        :array()),
    array('label'=>UserModule::t('View Profile'), 'url'=>array('/user/profile')),
    array('label'=>UserModule::t('Change password'), 'url'=>array('/user/profile/changepassword')),
);
?>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<div class="box box-info">
    <div class="box-body">
        <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
            'id'=>'profile-form',
            'enableAjaxValidation'=>true,
            'htmlOptions' => array('enctype'=>'multipart/form-data'),
        )); ?>

        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

        <?php echo $form->errorSummary(array($model,$detail)); ?>

        <?php
        $option = array('class' => 'span4',
            'rows' => 5,
            'options' => array('plugins' => array('clips', 'fontfamily'), 'lang' => 'sv')
        );
        ?>
        <div class="form-group">
            <?php echo $form->textFieldGroup($model,'username',array('placeholder'=>"Username",'class'=>'form-control')); ?>
        </div>

        <div class="form-group">
            <?php echo $form->emailFieldGroup($model,'email',array('placeholder'=>"Email", 'class'=>'form-control')); ?>
        </div>

        <?php if ($role == 'physician') { ?>
            <div class="form-group">
                <?php echo $form->textFieldGroup($detail,'name',array('placeholder'=>"Physician Name", 'class'=>'form-control')); ?>
            </div>

            <div class="form-group">
                <?php echo $form->textFieldGroup($detail,'manager_name',array('placeholder'=>"Manager Name", 'class'=>'form-control')); ?>
            </div>
        <?php } elseif ($role == 'agency') { ?>
            <div class="form-group">
                <?php echo $form->textFieldGroup($detail,'name',array('placeholder'=>"Agency Name", 'class'=>'form-control')); ?>
            </div>

            <div class="form-group">
                <?php echo $form->textFieldGroup($detail,'admin_name',array('placeholder'=>"Admin Name", 'class'=>'form-control')); ?>
            </div>

        <?php } ?>

        <?php echo $form->telFieldGroup($detail,'phone',array('placeholder'=>"Phone", 'class'=>'form-control')); ?>

        <?php echo $form->telFieldGroup($detail,'phone_fax',array('placeholder'=>"Phone Fax", 'class'=>'form-control')); ?>

        <?php echo $form->textFieldGroup($detail,'address',array('placeholder'=>"Address", 'class'=>'form-control')); ?>


        <?php if ($role == 'physician') { ?>

            <?php echo $form->emailFieldGroup($detail,'email',array('placeholder'=>"Contact Email Physician", 'class'=>'form-control')); ?>

            <?php echo $form->textFieldGroup($detail,'education',array('placeholder'=>"Education", 'class'=>'form-control')); ?>

            <?php echo $form->textFieldGroup($detail,'license',array('placeholder'=>"License", 'class'=>'form-control')); ?>

            <?php echo $form->numberFieldGroup($detail,'years_practice',array('placeholder'=>"Years practice", 'class'=>'form-control')); ?>


            <?php echo $form->redactorGroup(
                $detail,
                'specialization',
                array(
                    'widgetOptions' => array(
                        'editorOptions' => $option
                    )
                )
            ); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'summary',
                array(
                    'widgetOptions' => array(
                        'editorOptions' => $option
                    )
                )
            ); ?>

            <?php echo $form->fileFieldGroup($detail, 'photo'); ?>

        <?php } elseif ($role == 'agency') { ?>

            <?php echo $form->emailFieldGroup($detail,'agency_email',array('placeholder'=>"Contact Email Agency", 'class'=>'form-control')); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'skilled_nursing',
                array(
                    'widgetOptions' => array(
                        'editorOptions' => $option
                    )
                )
            ); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'physical_therapy',
                array(
                    array(
                        'widgetOptions' => array(
                            'editorOptions' => $option
                        )
                    )
                )
            ); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'speech_language_therapy',
                array(
                    array(
                        'widgetOptions' => array(
                            'editorOptions' => $option
                        )
                    )
                )
            ); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'occupational_therapy',
                array(
                    array(
                        'widgetOptions' => array(
                            'editorOptions' => $option
                        )
                    )
                )
            ); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'home_health_aide',
                array(
                    array(
                        'widgetOptions' => array(
                            'editorOptions' => $option
                        )
                    )
                )
            ); ?>

            <?php echo $form->redactorGroup(
                $detail,
                'medical_social_services',
                array(
                    array(
                        'widgetOptions' => array(
                            'editorOptions' => $option
                        )
                    )
                )
            ); ?>

            <?php echo $form->fileFieldGroup($detail, 'logo'); ?>
        <?php } ?>


        <div class="form-actions text-right">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => $model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save')
                )
            ); ?>
            <?php $this->widget(
                'booster.widgets.TbButton',
                array('buttonType' => 'reset', 'label' => 'Reset')
            ); ?>
        </div>

    <?php $this->endWidget();
    unset($form); ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->

