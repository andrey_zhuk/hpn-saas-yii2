<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('index'),
	$model->username,
);
$this->pageTitleContent = UserModule::t("Profile View");
$this->pageSubTitleContent = Yii::t('zii','Physician');

?>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body profile-card">
                    <div class="profile-picture">
                        <img src="<?php echo $model->physician->photo?>" width="200">
                    </div>
                    <div class="profile-overview-content">
                        <h2 class="text-left margin-left-8"><?php echo $model->physician->name?></h2>
                        <h5 class="lastactive"><?php echo "Last login: <i>". $last_visit ."</i>"?></h5>
                        <div class="box-body no-padding">
                            <table class="table table-noborder">
                                <?php if ($model->physician->manager_name): ?>
                                    <tr>
                                        <th><?php echo UserModule::t("Manager name");?></th>
                                        <td>
                                            <?php echo $model->physician->manager_name;  ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php if ($model->physician->address): ?>
                                <tr>
                                    <th><?php echo UserModule::t("Address");?></th>
                                    <td>
                                        <?php echo $model->physician->address;  ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <?php if ($model->physician->phone): ?>
                                    <tr>
                                        <th><?php echo UserModule::t("Phone");?></th>
                                        <td>
                                            <?php echo $model->physician->phone;  ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php if ($model->physician->phone_fax): ?>
                                    <tr>
                                        <th><?php echo UserModule::t("Phone Fax");?></th>
                                        <td>
                                            <?php echo $model->physician->phone_fax;  ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php if ($model->physician->email): ?>
                                    <tr>
                                        <th><?php echo UserModule::t("Email");?></th>
                                        <td>
                                            <?php echo $model->physician->email;  ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php if ($model->physician->summary): ?>
                                    <tr>
                                        <th><?php echo UserModule::t("Summary");?></th>
                                        <td>
                                            <?php echo $model->physician->summary;  ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php if ($model->physician->education): ?>
                                    <tr>
                                        <th><?php echo UserModule::t("Education");?></th>
                                        <td>
                                            <?php echo $model->physician->education;  ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                        </div><!-- /.box-body -->
                        <div class="btn-group pull-left margin-top-12">
                            <button type="button" class="btn btn-info btn-flat"><?php echo UserModule::t("Send message"); ?></button>
                            <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.box-header -->
            </div><!-- /.box -->

            <!-- Input addon -->
            <div class="box box-success">
                <div class="box-header">
                    <i class="fa fa-briefcase"></i><h3 class="box-title margin-left-2"><?php echo UserModule::t("Professional details"); ?></h3>
                </div>
                <div class="box-body">
                    <?php if ($model->physician->years_practice): ?>
                        <div class="Years practice">
                            <h4><?php echo UserModule::t("Years of practice");?></h4>
                            <span class="volunteering-interests-hint volunteering-interests-special-header"><?php echo $model->physician->years_practice; ?> years</span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->physician->license): ?>
                        <div class="Years practice">
                            <h4><?php echo UserModule::t("License");?></h4>
                            <span class="volunteering-interests-hint volunteering-interests-special-header"><?php echo $model->physician->license; ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->physician->education): ?>
                        <div class="Years practice">
                            <h4><?php echo UserModule::t("Education");?></h4>
                            <span class="volunteering-interests-hint volunteering-interests-special-header"><?php echo $model->physician->education; ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->physician->specialization): ?>
                        <div class="Years practice">
                            <h4><?php echo UserModule::t("Specialization");?></h4>
                            <span class="volunteering-interests-hint volunteering-interests-special-header"><?php echo $model->physician->specialization; ?></span>
                        </div>
                    <?php endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!--/.col (left) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
