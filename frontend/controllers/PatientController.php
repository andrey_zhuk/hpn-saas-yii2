<?php

namespace frontend\controllers;
use common\models\Agencys;
use app\models\OrdersRequest;
use app\models\PatientAssociateAgency;
use common\models\LoginData;
use common\user_exceptions\classes\UserNotFoundException;
use Yii;
use yii\web\BadRequestHttpException;
use app\components\Controller;
use common\models\ActivitiesPermitted;
use common\models\FunctionalLimitations;
use common\models\MentalStatus;
use app\models\PatientsDiagnosis;
use yii\helpers\ArrayHelper;
use common\models\Patients;
use app\models\PlanCare;
use app\models\FaceToFaceEncounter;
use common\models\MedicalNecessityList;
use common\models\Physician;
use app\models\AjaxValidation;
use yii\web\ForbiddenHttpException;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class PatientController extends Controller
{
    use AjaxValidation;
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['new-patient-referral-form'],
                        'roles' => [LoginData::TYPE_PHYSICIAN_NAME],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view-patient-referral-form'],
                        'roles' => [LoginData::TYPE_AGENCY_NAME],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['show','index','plan-of-care'],
                        'roles'   => ['@']
                    ],
                ]
            ],
        ];
    }

    public function actionIndex($id = 0)
    {
        if ($id === 0)
            throw new BadRequestHttpException;

        if ($this->userRole == LoginData::TYPE_PHYSICIAN_NAME)
            $with_array = ['physician', 'diagnosis', 'associateAgencyAccept', 'planOfCare', 'patientAgency'];
        elseif ($this->userRole == LoginData::TYPE_AGENCY_NAME)
            $with_array = ['physician', 'diagnosis', 'planOfCare', 'agency', 'patientAgency'];

        $patient = Patients::find()
            ->where(['id' => $id])
            ->with($with_array)
            ->one();

        if ($patient == NULL || ($this->userRole == LoginData::TYPE_PHYSICIAN_NAME && $patient->id_physician != \Yii::$app->user->id)
            || ($this->userRole == LoginData::TYPE_AGENCY_NAME &&
                ($patient->agency == NULL || $patient->agency->id != \Yii::$app->user->id)))
            throw new ForbiddenHttpException;

        $planOfCare = ($patient->planOfCare == NULL) ? new PlanCare() : $patient->planOfCare;

        $face_to_face = FaceToFaceEncounter::findOne(['id_patient' => $id]);
        if ($face_to_face == NULL) $face_to_face = new FaceToFaceEncounter();

        if(Yii::$app->request->isAjax && $_POST['ajax'] == 'plan-of-care') {
            $patient->setScenario(Patients::SCENARIO_PATIENT_1);
            $this->performAjaxValidation($patient, $planOfCare);
        }

        if(isset($_POST['PlanCare'])) {
            $post = \Yii::$app->request->post();
            $patient->load($post);
            $planOfCare->load($post);

            $patient->setScenario(Patients::SCENARIO_PATIENT_1);

            if (!ActiveForm::validate($patient, $planOfCare)) {
                if($patient->save()){
                    $planOfCare->id_patient = $patient->id;
                    $planOfCare->save();
                    $this->redirect(['/patient/index', 'id'=>$id]);
                }
            }
            else {
                echo json_encode(ActiveForm::validate($patient, $planOfCare));
                \Yii::$app->end();
            }
        }

        if(isset($_POST['FaceToFaceEncounter'])) {
            $post = \Yii::$app->request->post();
            $patient->birthday = $post['Patients']['birthday'];
            $patient->name = $post['Patients']['name'];

            $face_to_face->load($post);

            $patient->setScenario(Patients::SCENARIO_PATIENT_2);
            if (!ActiveForm::validate($patient, $face_to_face)) {
                if($patient->save()) {
                    $face_to_face->id_patient = $patient->id;
                    $face_to_face->save();
                    $this->redirect(['/patient/index', 'id'=>$id]);
                }
            }
            else {
                echo json_encode(ActiveForm::validate($patient, $face_to_face));
                \Yii::$app->end();
            }
        }

        $prognosis = $planOfCare->prognosis_array;

        $fun_limitations = FunctionalLimitations::find()->all();
        $funlim_array = ArrayHelper::index($fun_limitations, 'id');

        $activities_permitted = ActivitiesPermitted::find()->all();
        $actperm_array = ArrayHelper::index($activities_permitted, 'id');

        $mental_status = MentalStatus::find()->all();
        $mental_status_array = ArrayHelper::index($mental_status, 'id');

        $medical_necessity_list = MedicalNecessityList::find()->all();
        foreach($medical_necessity_list as $list)
            $necessity_list[$list->id] = $list->name;

        $order_patient = OrdersRequest::find()
            ->where('id_patient = :id_patient', ['id_patient' => $id])
            ->orderBy('date_create')
            ->all();

        $data['gender'] = [Patients::GENDER_MALE => Yii::t('app', 'Male'), Patients::GENDER_FEMALE => Yii::t('app', 'Female')];

        ($this->userRole == LoginData::TYPE_PHYSICIAN_NAME && $planOfCare->isNewRecord == true) ? $data['save_form'] = true : $data['save_form'] = false;
        ($this->userRole == LoginData::TYPE_PHYSICIAN_NAME && $face_to_face->isNewRecord == true) ? $data['save_face_to_face'] = true : $data['save_face_to_face'] = false;

        return $this->render('index', [
            'necessity_list' => $necessity_list,
            'face_to_face' => $face_to_face,
            'patient' => $patient,
            'plan_care' => $planOfCare,
            'agencys' => $patient->agency,
            'physician' => $patient->physician,
            'fun_limitations' => $funlim_array,
            'activities_permitted' => $actperm_array,
            'mental_status' => $mental_status_array,
            'prognosis' => $prognosis,
            'data' => $data,
            'order_patient' => $order_patient,
            'role' => $this->userRole
        ]);
    }

    public function actionNewPatientReferralForm($shortId = 0)
    {
        if ($shortId === 0)
            throw new UserNotFoundException();

        $user = LoginData::findByShortId($shortId);

        $agencys = $user->userData;
        if ($user == NULL || $user->type != LoginData::TYPE_AGENCY)
            throw new BadRequestHttpException;

        $patient = new Patients();
        $patient_diagnosis = new PatientsDiagnosis();
        $face_to_face = new FaceToFaceEncounter();
        $plan_care = new PlanCare();

        $patient->setScenario(Patients::SCENARIO_PATIENT_0);
        $physician = $this->user->userData;

        $fun_limitations = FunctionalLimitations::find()->all();
        $funlim_array = ArrayHelper::index($fun_limitations, 'id');

        $activities_permitted = ActivitiesPermitted::find()->all();
        $actperm_array = ArrayHelper::index($activities_permitted, 'id');

        $mental_status = MentalStatus::find()->all();
        $mental_status_array = ArrayHelper::index($mental_status, 'id');

        $data['gender'] = [Patients::GENDER_MALE => Yii::t('app', 'Male'), Patients::GENDER_FEMALE => Yii::t('app', 'Female')];
        $data['save_form'] = true;
        $data['save_face_to_face'] = true;

        $medical_necessity_list = MedicalNecessityList::find()->all();
        $necessity_list = ArrayHelper::map($medical_necessity_list, 'id', 'name');

        $prognosis = $plan_care->prognosis_array;
        $necessary_list = $patient->necessary;

        if(Yii::$app->request->isAjax && $_POST['ajax'] == 'plan-of-care') {
            $patient->setScenario(Patients::SCENARIO_PATIENT_1);
            $this->performAjaxValidation($patient, $plan_care);
        }

        if(Yii::$app->request->isAjax && $_POST['ajax'] == 'referral-form') {
            $this->performAjaxValidation($patient, $patient_diagnosis);
        }

        if(isset($_POST['PlanCare'])) {
            $patient->load(\Yii::$app->request->post());
            $patient->id_physician = \Yii::$app->user->id;
            $patient->date_create = time();

            $plan_care->load(\Yii::$app->request->post());

            $patient->setScenario(Patients::SCENARIO_PATIENT_1);

            if (!ActiveForm::validate($patient,$plan_care)) {

                if($patient->save()){
                    $plan_care->id_patient = $patient->id;
                    $plan_care->save();

                    $associate_agency = new PatientAssociateAgency();
                    $associate_agency->id_agency = $agencys->id;
                    $associate_agency->id_patient = $patient->id;
                    $associate_agency->status_send = PatientAssociateAgency::STATUS_SEND;
                    $associate_agency->date_send = time();
                    $associate_agency->save();

                    $this->redirect(['/physician']);
                }
            }
            else {
                echo json_encode(ActiveForm::validate($patient, $plan_care));
                \Yii::$app->end();
            }
        }
        elseif(isset($_POST['PatientsDiagnosis'])) {
            $patient->load(\Yii::$app->request->post());
            $patient->id_physician = \Yii::$app->user->id;
            $patient->date_create = time();

            $patient->setScenario(Patients::SCENARIO_PATIENT_1);
            $patient_diagnosis->load(\Yii::$app->request->post());

            if (!ActiveForm::validate($patient,$patient_diagnosis)) {
                if($patient->save()) {
                    $patient_diagnosis->patient_id = $patient->id;
                    $patient_diagnosis->date_primary_diagnosis = time();
                    $patient_diagnosis->save();

                    $associate_agency = new PatientAssociateAgency();
                    $associate_agency->id_agency = $agencys->id;
                    $associate_agency->id_patient = $patient->id;
                    $associate_agency->status_send = PatientAssociateAgency::STATUS_SEND;
                    $associate_agency->date_send = time();
                    $associate_agency->save();

                    $this->redirect(['/physician']);
                }
            }
            else {
                echo json_encode(ActiveForm::validate($patient, $patient_diagnosis));
                \Yii::$app->end();
            }
        }
        elseif(isset($_POST['FaceToFaceEncounter'])) {
            $patient->load(\Yii::$app->request->post());
            $patient->id_physician = \Yii::$app->user->id;
            $patient->date_create = time();

            $face_to_face->load(\Yii::$app->request->post());

            $patient->setScenario(Patients::SCENARIO_PATIENT_2);
            if (!ActiveForm::validate($patient,$face_to_face)) {
                if($patient->save()) {
                    $face_to_face->id_patient = $patient->id;
                    $face_to_face->save();

                    $associate_agency = new PatientAssociateAgency();
                    $associate_agency->id_agency = $agencys->id;
                    $associate_agency->id_patient = $patient->id;
                    $associate_agency->status_send = PatientAssociateAgency::STATUS_SEND;
                    $associate_agency->date_send = time();
                    $associate_agency->save();

                    $this->redirect(['/physician']);
                }
            }
            else {
                echo json_encode(ActiveForm::validate($patient, $face_to_face));
                \Yii::$app->end();
            }
        }


        return $this->render('send_patient_info', [
            'patient' => $patient,
            'physician' => $physician,
            'agencys' => $agencys,
            'patient_diagnosis' => $patient_diagnosis,
            'necessary' => $necessary_list,
            'face_to_face' => $face_to_face,
            'plan_care' => $plan_care,
            'fun_limitations' => $funlim_array,
            'activities_permitted' => $actperm_array,
            'mental_status' => $mental_status_array,
            'prognosis' => $prognosis,
            'necessity_list' => $necessity_list,
            'data' => $data
        ]);
    }

    public function actionViewPatientReferralForm($id = 0)
    {
        $patient_id = $id;
        if ($patient_id === 0)
            throw new UserNotFoundException();

        $patient = Patients::find()->with('physician')->where(['id' => $patient_id])->one();
        if ($patient == NULL)
            throw new UserNotFoundException();

        $agencys = Agencys::find()->where(['agencys.id' => \Yii::$app->user->id])
            ->join('INNER JOIN','patient_associate_agency associate', 'associate.id_agency = agencys.id and associate.id_patient = '.$patient_id.' and
            ((associate.status_send = '.PatientAssociateAgency::STATUS_REJECT.' and associate.date_end_accept > '.time().')
            or (associate.status_send != '.PatientAssociateAgency::STATUS_REJECT.'))')->one();
        if ($agencys == NULL)
            throw new BadRequestHttpException;

        $patient_diagnosis = PatientsDiagnosis::findOne(['patient_id' => $patient_id]);
        $face_to_face = FaceToFaceEncounter::findOne(['id_patient' => $patient_id]);
        $plan_care = PlanCare::findOne(['id_patient' => $patient_id]);

        $prognosis = "";
        if ($plan_care != NULL)
            $prognosis = $plan_care->prognosis_array;

        $fun_limitations = FunctionalLimitations::find()->all();
        $funlim_array = ArrayHelper::index($fun_limitations, 'id');

        $activities_permitted = ActivitiesPermitted::find()->all();
        $actperm_array = ArrayHelper::index($activities_permitted, 'id');

        $mental_status = MentalStatus::find()->all();
        $mental_status_array = ArrayHelper::index($mental_status, 'id');

        $data['gender'] = [Patients::GENDER_MALE => Yii::t('app', 'Male'), Patients::GENDER_FEMALE => Yii::t('app', 'Female')];
//        var_dump($patient); die();

        $medical_necessity_list = MedicalNecessityList::find()->all();
        foreach($medical_necessity_list as $list) {
            $necessity_list[$list->id] = $list->name;
        }

        $necessary_list = $patient->necessary;

        return $this->render('view_patient_info', [
            'patient' => $patient,
            'physician' => $patient->physician,
            'agencys' => $agencys,
            'patient_diagnosis' => $patient_diagnosis,
            'necessary' => $necessary_list,
            'face_to_face' => $face_to_face,
            'plan_care' => $plan_care,
            'fun_limitations' => $funlim_array,
            'activities_permitted' => $actperm_array,
            'mental_status' => $mental_status_array,
            'prognosis' => $prognosis,
            'necessity_list' => $necessity_list,
            'data' => $data
        ]);
    }

    public function actionFaceToFaceCreate()
    {
        $encounter = new FaceToFaceEncounter;
        $medical_necessity_list = MedicalNecessityList::find()->all();
        $physician = Physician::find()
            ->where('id_user = :userid', [':userid' => \Yii::$app->user->id])->one();

        foreach($medical_necessity_list as $list) {
            $necessity_list[$list->id] = $list->name;
        }

        if (isset($_GET['id'])) {
            $patient = Patients::find()
                ->where('id = :id', [':id' => $_GET['id']])->one();
        }
        else {
            $patient = new Patients;
        }

        return $this->render('face_to_face_page', [
            'patient' => $patient,
            'encounter' => $encounter,
            'necessity_list' => $necessity_list,
            'physician' => $physician
        ]);
    }

    public function actionPlanOfCare($id = 0)
    {
        if ($id === 0)
            throw new UserNotFoundException();

        $patient_id = $_GET['id'];

        if ($this->userRole == LoginData::TYPE_PHYSICIAN_NAME) {
            $patient = Patients::find()
                ->where(['id' => $patient_id])
                ->with(['physician'])
                ->one();
            $physician = $patient->physician;

            $associate_agency = PatientAssociateAgency::find()
                ->where(['id_patient' => $patient_id])
                ->with('agency')
                ->one();
            $agencys = (isset($associate_agency->agency)) ? $associate_agency->agency : NULL;
            $data['save_form'] = true;
        }
        elseif ($this->userRole == LoginData::TYPE_AGENCY_NAME) {
            $patient = Patients::find()
                ->where(['id' => $patient_id])
                ->with(['physician'])
                ->one();
            $physician = $patient->physician;

            $associate_agency = PatientAssociateAgency::find()
                ->where(['id_patient' => $patient_id, 'status_send' => PatientAssociateAgency::STATUS_ACCEPT])
                ->with('agency')
                ->one();
            $agencys = (isset($associate_agency->agency)) ? $associate_agency->agency : NULL;
            $data['save_form'] = false;
        }

        $plan_care = PlanCare::find()
            ->where(['id_patient' => $patient_id])
            ->one();
        if (!$plan_care)
            $plan_care = new PlanCare();

        $prognosis = $plan_care->prognosis_array;

        $fun_limitations = FunctionalLimitations::find()->all();
        $funlim_array = ArrayHelper::index($fun_limitations, 'id');

        $activities_permitted = ActivitiesPermitted::find()->all();
        $actperm_array = ArrayHelper::index($activities_permitted, 'id');

        $mental_status = MentalStatus::find()->all();
        $mental_status_array = ArrayHelper::index($mental_status, 'id');

        $data['gender'] = [Patients::GENDER_MALE => Yii::t('app', 'Male'), Patients::GENDER_FEMALE => Yii::t('app', 'Female')];

        if(isset($_POST['PlanCare'])) {
            $plan_care->load(\Yii::$app->request->post());

            $plan_care->id_patient = $patient_id;
            $plan_care->save();

            return $this->goBack();
        }

        return $this->render('plan_of_care_page', [
            'role' => $this->userRole,
            'patient' => $patient,
            'physician' => $physician,
            'agencys' => $agencys,
            'plan_care' => $plan_care,
            'fun_limitations' => $funlim_array,
            'activities_permitted' => $actperm_array,
            'mental_status' => $mental_status_array,
            'prognosis' => $prognosis,
            'data' => $data
        ]);
    }

    public function actionNewReferralForm()
    {
        if (!isset($_GET['id']))
            throw new BadRequestHttpException(Yii::t("app", "The specified post cannot be found."));

        $patient = new Patients();
        $patient_diagnosis = new PatientsDiagnosis();
        $face_to_face = new FaceToFaceEncounter();
        $physician = Physician::findOne(['id_user' => Yii::$app->user->id]);

        $necessary_list = $patient->necessary;

        $this->performAjaxValidation($patient, $patient_diagnosis);

        if(Yii::$app->request->post()) {
            $patient->load(\Yii::$app->request->post());
            $patient->id_physician = \Yii::$app->user->id;
            $patient->date_create = time();
            $patient->save();

            $patient_diagnosis->load(\Yii::$app->request->post());
            $patient_diagnosis->patient_id = $patient->id;
            $patient_diagnosis->date_primary_diagnosis = time();
            $patient_diagnosis->necessary_home_services = json_encode($patient_diagnosis->necessary_home_services);

            if ($patient_diagnosis->save()) {
                $associate_agency = new PatientAssociateAgency();
                $associate_agency->id_agency = $_GET['id'];
                $associate_agency->id_patient = $patient->id;
                $associate_agency->status_send = PatientAssociateAgency::STATUS_SEND;
                $associate_agency->date_send = time();
                $associate_agency->save();
            }

            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Referral to agency is sent successfully'));
            $this->redirect(['/physician']);
        }

        return $this->render('referal_form_page', [
            'patient' => $patient,
            'patient_diagnosis' => $patient_diagnosis,
            'necessary' => $necessary_list,
            'face_to_face' => $face_to_face,
            'physician' => $physician
        ]);
    }
}
