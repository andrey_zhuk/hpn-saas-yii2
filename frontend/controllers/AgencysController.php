<?php

namespace frontend\controllers;

use app\models\OrdersRequest;
use app\models\PatientAssociateAgency;
use common\models\Patients;
use app\models\PlanCareRequest;
use Yii;
use common\models\Agencys;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use yii\helpers\Url;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AgencysController implements the CRUD actions for Agencys model.
 */
class AgencysController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'new-patient-referrals', 'discharged-patients', 'current-patients', 'order-requests'],
                        'roles' => ['agency'],
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Agencys models.
     * @return mixed
     */
    public function actionIndex()
    {
        $patient_coming = Patients::find()
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and
                (associate.status_send = '.PatientAssociateAgency::STATUS_SEND.' or (associate.status_send = ' .
                PatientAssociateAgency::STATUS_REJECT.' and associate.date_end_accept > '.time().'))')
            ->joinWith(['associateAgency'])
            ->where(['patient_associate_agency.id_agency' => \Yii::$app->user->id])
            ->with(['physician'])
            ->orderBy(['patient_associate_agency.date_send' => SORT_ASC])
            ->all();

//        $notification_plancare = PlanCareRequest::find()
//            ->where('id_agency = :id_agency and date_read_approved IS NULL', ['id_agency' => \Yii::$app->user->id])
//            ->with(['patients', 'physician', 'profile'])
//            ->all();
//
//        foreach ($notification_plancare as $plancare) {
//            $data['patient_id'] = $plancare->patients->id;
//            $data['patient_name'] = $plancare->patients->name;
//            $data['physician_id'] = $plancare->physician->id;
//            $data['physician_name'] = $plancare->physician->name;
//
//            if ($plancare->date_read_approved == NULL && $plancare->date_resend != NULL) {
//                $data['date'] = date('m/d/Y', (int)$plancare->date_resend);
//                $data['button'] = Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/index', 'id' => $plancare->patients->id, 'tab' => 'plan-of-care'], true), [
//                    'title' => Yii::t('app', "View Plan Of Care"),
//                    'data-toggle' => "tooltip",
//                    'data-placement' => "bottom",
//                    'class' => 'btn btn-sm bg-orange'
//                ]);
//            } elseif($plancare->date_resend == NULL) {
//                $data['date'] = date('m/d/Y', (int)$plancare->date_send);
//                $data['button'] = "<span class='label label-warning'>".Yii::t('app', "awaiting for physician's approval")."</span>";
//            }
//            array_push($order_array, $data);
//        }
        $patient_queue = $this->getPatientQueue();

        return $this->render('index', [
            'patient_coming' => $patient_coming,
            'patient_queue' => $patient_queue
        ]);
    }

    protected function getPatientQueue() {
        $patient_array = [];
        $patient = Patients::find()
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and associate.status_send = '.PatientAssociateAgency::STATUS_ACCEPT)
            ->joinWith(['associateAgency'])
            ->where(['patient_associate_agency.id_agency' => \Yii::$app->user->id, 'status_care' => Patients::STATUS_CARE])
            ->with(['physician', 'faceToFace', 'planOfCare'])
            ->all();

        foreach ($patient as $patient_detail) {
            if (!isset($patient_detail->faceToFace) || !isset($patient_detail->planOfCare)) {
                $data['patient_id'] = $patient_detail->id;
                $data['patient_name'] = $patient_detail->name;
                $data['physician_id'] = (isset($patient_detail->physician)) ? $patient_detail->physician->id : "";
                $data['physician_name'] = (isset($patient_detail->physician)) ? $patient_detail->physician->name : "";
                $data['physician_short_id'] = (isset($patient_detail->physician->user)) ? $patient_detail->physician->user->short_id : "";

                $data['date'] = $patient_detail->date_create + 2592000;
                $data['button'] = '';
                $data['status'] = "";
                if (!isset($patient_detail->faceToFace)) {
                    $data['status'] .= '<small class="label label-info">'.Yii::t('app','Face To Face Not Yet Sent').'</small>';
                    $data['status'] .= '&nbsp';
                }
                if (!isset($patient_detail->planOfCare)) {
                    $data['status'] .= '<small class="label label-primary">'.Yii::t('app','POC Not Yet Sent').'</small>';
                }
                array_push($patient_array, $data);
            }
        }

        $order_patient = OrdersRequest::find()
            ->where('id_agency = :id_agency and date_accept IS NULL', ['id_agency' => \Yii::$app->user->id])
            ->with(['patients', 'physician'])
            ->all();

        foreach ($order_patient as $order) {
            if ($order->date_read_agency == NULL) {
                $data['patient_id'] = $order->patients->id;
                $data['patient_name'] = $order->patients->name;
                $data['physician_id'] = $order->physician->id;
                $data['physician_name'] = $order->physician->name;
                $data['physician_short_id'] = (isset($order->physician->user)) ? $order->physician->user->short_id : "";

                $data['button'] = "";
                $data['status'] = "";
                $data['date'] = $order->date_create;
                if ($order->type == OrdersRequest::AGENCY_SEND && $order->date_resend == NULL) {
                    $data['status'] .= "<span class='label label-warning'>" . Yii::t('app', "awaiting for physician's approval") . "</span>";
                } elseif ($order->type == OrdersRequest::PHYSICIAN_SEND && $order->date_accept == NULL && $order->date_read_agency == NULL) {
                    $data['status'] .= "<span class='label label-warning'>" . Yii::t('app', "New Physician Medication/Treatment Orders") . "</span>";
                    $data['button'] .= Html::a(Yii::t('app', "Order Update"), Url::toRoute(['patient/index', 'id' => $order->patients->id, 'tab' => 'patient-orders'], true), [
                        'title' => Yii::t('app', "View Order Update"),
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'class' => 'btn btn-xs btn-primary'
                    ]);
                } elseif ($order->type == OrdersRequest::AGENCY_SEND && $order->date_resend != NULL) {
                    $data['status'] .= "<span class='label label-warning'>" . Yii::t('app', "returned Physician Medication/Treatment Orders") . "</span>";
                    $data['button'] .= Html::a(Yii::t('app', "Order Update"), Url::toRoute(['patient/index', 'id' => $order->patients->id, 'tab' => 'patient-orders'], true), [
                        'title' => Yii::t('app', "View Order Update"),
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'class' => 'btn btn-xs btn-primary'
                    ]);
                    $data['date'] = $order->date_resend;
                }

                array_push($patient_array, $data);
            }
        }
        if (count($patient_array) != 0) {
            foreach ($patient_array as $key => $arr) {
                $data_time[$key] = $arr['date'];
            }
            array_multisort($data_time, SORT_DESC, SORT_NUMERIC, $patient_array);
        }

        return $patient_array;
    }


    /**
     * Displays a single Agencys model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agencys model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agencys();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Agencys model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Agencys model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Agencys model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agencys the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agencys::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionNewPatientReferrals()
    {
        $patient_coming = Patients::find()
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and
                (associate.status_send = '.PatientAssociateAgency::STATUS_SEND.' or (associate.status_send = '.PatientAssociateAgency::STATUS_REJECT.' and associate.date_end_accept > '.time().'))')
            ->joinWith(['associateAgency'])
            ->where(['patient_associate_agency.id_agency' => \Yii::$app->user->id])
            ->with(['physician'])
            ->orderBy(['patient_associate_agency.date_send' => SORT_ASC])
            ->all();

        return $this->render('referrals', [
            'patient_coming' => $patient_coming,
        ]);
    }

    public function actionDischargedPatients()
    {
        $patient_care_end = Patients::find()->select('patients.*, ')
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and associate.status_send = '.PatientAssociateAgency::STATUS_ACCEPT)
            ->joinWith(['associateAgency'])
            ->where(['patient_associate_agency.id_agency' => \Yii::$app->user->id, 'status_care' => Patients::STATUS_CARE_END])
            ->with(['physician'])
            ->orderBy(['patient_associate_agency.date_send' => SORT_ASC])
            ->all();

        return $this->render('discharged', [
            'patient_care_end' => $patient_care_end,
        ]);
    }

    public function actionCurrentPatients()
    {
        $patient_care = Patients::find()->select('patients.*, ')
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and associate.status_send = '.PatientAssociateAgency::STATUS_ACCEPT)
            ->joinWith(['associateAgency'])
            ->where(['patient_associate_agency.id_agency' => \Yii::$app->user->id, 'status_care' => Patients::STATUS_CARE])
            ->with(['physician'])
            ->orderBy(['patient_associate_agency.date_send' => SORT_ASC])
            ->all();

        $patient_queue = $this->getPatientQueue();

        return $this->render('current', [
            'patient_care' => $patient_care,
            'patient_queue' => $patient_queue
        ]);
    }

    public function actionOrderRequests()
    {
        $order_array = [];
        $order_patient = OrdersRequest::find()
            ->where('id_agency = :id_agency and date_accept IS NULL', ['id_agency' => \Yii::$app->user->id])
            ->with(['patients', 'physician'])
            ->all();

        foreach ($order_patient as $order) {
            $data['patient_id'] = $order->patients->id;
            $data['patient_name'] = $order->patients->name;
            $data['physician_id'] = $order->physician->id;
            $data['physician_name'] = $order->physician->name;
            $data['physician_phone'] = $order->physician->phone;
            $data['physician_short_id'] = (isset($order->physician->user)) ? $order->physician->user->short_id : "";
            $data['text'] = $order->text;

            $data['button'] = "<a class='accept-patient btn btn-sm bg-olive' data-placement='bottom' title='". Yii::t('app', 'Accept Order') ."' data-toggle='tooltip' href='javascript:;' onclick='App.Api.acceptOrder(".$order->id.",this);'>".Yii::t('app', 'Accept')."</a>";
            if ($order->type == OrdersRequest::AGENCY_SEND && $order->date_resend != NULL && $order->date_accept == NULL) {
                $data['date'] = $order->date_resend;
            } elseif($order->type == OrdersRequest::PHYSICIAN_SEND && $order->date_accept == NULL) {
                $data['date'] = $order->date_create;
            } elseif($order->type == OrdersRequest::AGENCY_SEND && $order->date_resend == NULL) {
                $data['date'] = $order->date_create;
                $data['button'] = "<span class='label label-warning'>".Yii::t('app', "awaiting for physician's approval")."</span>";
            }

            array_push($order_array, $data);
        }

        $notification_plancare = PlanCareRequest::find()
            ->where('id_agency = :id_agency and date_read_approved IS NULL', ['id_agency' => \Yii::$app->user->id])
            ->with(['patients', 'physician'])
            ->all();

        foreach ($notification_plancare as $plancare) {
            $data['patient_id'] = $plancare->patients->id;
            $data['patient_name'] = $plancare->patients->name;
            $data['physician_id'] = $plancare->physician->id;
            $data['physician_name'] = $plancare->physician->name;
            $data['physician_phone'] = $plancare->physician->phone;
            $data['physician_short_id'] = (isset($plancare->physician->user)) ? $plancare->physician->user->short_id : "";
            $data['text'] = "Update Plan Of Care";

            if ($plancare->date_read_approved == NULL && $plancare->date_resend != NULL) {
                $data['date'] = $plancare->date_resend;
                $data['button'] = Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/index', 'id' => $plancare->patients->id, 'tab' => 'plan-of-care'], true), [
                    'title' => Yii::t('app', "View Plan Of Care"),
                    'data-toggle' => "tooltip",
                    'data-placement' => "bottom",
                    'class' => 'btn btn-sm bg-orange'
                ]);
            } elseif($plancare->date_resend == NULL) {
                $data['date'] = $plancare->date_send;
                $data['button'] = "<span class='label label-warning'>".Yii::t('app', "awaiting for physician's approval")."</span>";
            }
            array_push($order_array, $data);
        }

        if (count($order_array) != 0) {
            foreach ($order_array as $key => $arr) {
                $data_time[$key] = $arr['date'];
            }
            array_multisort($data_time, SORT_DESC, SORT_NUMERIC, $order_array);
        }


        return $this->render('order_request', [
            'order_array' => $order_array,
        ]);
    }
}
