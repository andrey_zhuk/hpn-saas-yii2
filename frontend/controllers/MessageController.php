<?php

namespace frontend\controllers;

use app\models\ChatMembers;
use app\models\Message;
use app\models\UserMessages;
use app\models\Chats;
use common\models\Agencys;
use common\models\LoginData;
use common\models\Physician;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use frontend\controllers\SmileyContoller;
use app\components\Controller;

class MessageController extends Controller
{
    private $per_page = 4;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'read-messages', 'get-count-unread-messages', 'update-message', 'get-member-chat',
                            'remove-message', 'get-latest-members', 'save-message', 'get-unread-messages'],
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    public function actionIndex($shortId = 0)
    {
        if($shortId == $this->user->short_id){
            $this->redirect(array('/'));
        }

        $member = LoginData::findByShortId($shortId);
        $chat = self::actionInitUserChat($member->id);

//        if ($this->userRole == LoginData::TYPE_AGENCY_NAME)
//            $model = Agencys::find()->where(['id' => \Yii::$app->user->id])->with('user')->one();
//
//        elseif ($this->userRole == LoginData::TYPE_PHYSICIAN_NAME)
//            $model = Physician::find()->where(['id' => \Yii::$app->user->id])->with('user')->one();

//        $profile = Profile::find()->where(['user_id' => \Yii::$app->user->id])->one();

        $model = LoginData::findByShortId($this->user->short_id);
//            Profile::find()->where(['user_id' => $id])->one();

        //print_r($this->actionGetLatestMembers());
        //die;
//        var_dump($this->actionGetLatestMembers()); die();
        return $this->render('index', [
            'model'=>$model,
            'member' => $member,
            'chat' => $chat,
            'latest_members' => $this->actionGetLatestMembers(),
            'role' => $this->userRole
        ]);
    }

    /**
     * Init chat with user
     *
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function actionInitUserChat($id){
        $chat_id = self::actionCheckExistChat($id);

        if (!$chat_id) {
            $chat_id = self::actionInitChat();
            self::actionAddMemberToChat($id, $chat_id);
        }

        $chat = self::actionGetChat($chat_id);
        return $chat;
    }

    /**
     * Init new chat
     *
     * @return int
     */
    public static function actionInitChat()
    {
        $chat = new Chats;
        $chat->name = 'test chat';
        $chat->save();

        self::actionAddMemberToChat(\Yii::$app->user->id, $chat->id);
        return $chat->id;
    }

    /**
     * Get data chat
     *
     * @param $chat_id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function actionGetChat($chat_id)
    {
        $chat = Chats::find()->where(['id' => $chat_id])->with('members')->one();
        return $chat;
    }

    /**
     * Check if user has access do any action with message or chat
     *
     * @param bool|false $chat_id
     * @param bool|false $message_id
     * @return array|null|\yii\db\ActiveRecord|\yii\db\Query
     */
    public function checkAccessAction($chat_id = false, $message_id = false)
    {
        if($chat_id) {
            return ChatMembers::find()->where(['chat_id' => $chat_id, 'user_id' => \Yii::$app->user->id])->one();
        }
        if($message_id) {
            return Message::find()->where(['message_id' => $message_id])
                            ->innerJoin('chat_members', 'chat_members.chat_id = message.chat_id AND
                                         chat_members.user_id = ' . \Yii::$app->user->id);
        }
        return false;
    }

    /**
     * Check if already chat exist, if exist then return chat id
     *
     * @param $member_id
     * @return bool|mixed
     */
    public static function actionCheckExistChat($member_id)
    {
        $chat = Chats::find()
                ->innerJoin('chat_members `cm`', '`cm`.`user_id` = ' . \Yii::$app->user->id . ' AND cm.chat_id = chats.id')
                ->innerJoin('chat_members `cm2`', '`cm2`.`user_id` = ' . $member_id. ' AND cm2.chat_id = chats.id')
                ->one();

        if($chat){
            return $chat->id;
        }
        return false;
    }

    /**
     * Add new member(user) to chat
     *
     * @param $user_id
     * @param $chat_id
     * @throws \Exception
     */
    public static function actionAddMemberToChat($user_id, $chat_id)
    {
        $chat_member = new ChatMembers();
        $chat_member->user_id = $user_id;
        $chat_member->chat_id = $chat_id;
        $chat_member->save();
    }

    /**
     * Get members(user) chat
     *
     * @param $chat_id
     * @return mixed
     */
    public function actionGetMemberChat($chat_id)
    {
        return ChatMembers::find()->where(['chat_id' => $chat_id])->all();
    }

    /**
     * Get latest members(users)
     *
     * @return mixed
     */
    public function actionGetLatestMembers()
    {
        return ChatMembers::find()->select('cm.*')->where(['chat_members.user_id' => \Yii::$app->user->id])
            ->innerJoin('chat_members as cm', 'cm.chat_id = chat_members.chat_id and cm.user_id <> '. \Yii::$app->user->id)
            ->groupBy('cm.user_id')->with(['user'])->all();
    }

    /**
     * Save new message
     *
     * @param $chat_id
     * @param $message
     * @return Message
     */
    public function actionSaveMessage($chat_id, $message)
    {
        if(!$this->checkAccessAction($chat_id)) {
            return false;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Message();
        $model->chat_id = $chat_id;
        $model->message = $message;
        $model->status = 1;
        if ($model->save()) {

            /* save message for each member chat */
            $members = $this->actionGetMemberChat($chat_id);
            foreach($members as $member) {
                $read = $member->user_id == \Yii::$app->user->id ? 1 : 0;
                $user_message = new UserMessages();
                $user_message->sender = \Yii::$app->user->id;
                $user_message->recipient = $member->user_id;
                $user_message->message_id = $model->id;
                $user_message->status = 1;
                $user_message->read = $read;
                $user_message->save();
            }
            $message = Message::find()->with(['sender', 'chat'])->where(['id' => $model->id])->asArray()->one();
            return $message;
        }
        return ['errors' => true];
    }

    /**
     * Remove user message(set message status)
     *
     * @param $message_id
     * @return bool
     * @throws \Exception
     */
    public function actionRemoveMessage($message_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if(!$this->checkAccessAction(false, $message_id)) {
            return false;
        }

        /* check user access update message */
        $message = UserMessages::find()->where(['message_id' => $message_id, 'recipient' => \Yii::$app->user->id])->one();
        $message->status = 0;
        if (!$message->update()) {
            return true;
        }
        return false;
    }

    /**
     * Update chat message
     *
     * @param $message_id
     * @return bool
     * @throws \Exception
     */
    public function actionUpdateMessage($text, $message_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if(!$this->checkAccessAction(false, $message_id)) {
            return false;
        }

        /* check user access update message */
        $message = Message::findOne($message_id);
        $message->message = $text;
        if ($message->update()) {
            return $message;
        }
        return false;
    }

    /**
     * Get unread messages
     * @param @user_id
     * @return mixed
     */
    public function actionGetUnreadMessages($user_id = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user_id = $user_id ? $user_id : \Yii::$app->user->id;
        $messages['messages'] = Message::find()->with(['sender', 'recipient'])
                            ->innerJoin('user_messages', 'user_messages.recipient = ' .  $user_id . ' AND
                                user_messages.message_id = message.id AND user_messages.read = 0' )
                            ->asArray()->limit(10)->orderBy(['message.id' => SORT_DESC])->all();

        $user = [];
        foreach ($messages['messages'] as $message) {
            $user_array = [$message['sender']['sender'], $message['sender']['recipient'], $message['recipient']['sender'], $message['recipient']['recipient']];

            foreach ($user_array as $user_detail) {
                if (!isset($message['users'][$user_detail])) {
                    $login_data = LoginData::findOne($user_detail);
                    $user_data = $login_data->userData;
                    $messages['users'][$login_data->id] = ArrayHelper::toArray($user_data);
                    $messages['users'][$login_data->id]['short_id'] = $login_data->short_id;
                }
            }
        }
        return $messages;
    }

    /**
     * Get unread messages
     * @param @user_id
     * @return mixed
     */
    public static function actionGetCountUnreadMessages($user_id = false, $chat_id = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $user_id = $user_id ? $user_id : \Yii::$app->user->id;

        $count = Message::find();

        if($chat_id) {
            $count = $count->where(['chat_id' => $chat_id]);
        }
        $count = $count->innerJoin('user_messages','message.id = user_messages.message_id  AND
                                                     user_messages.recipient = ' . $user_id . ' AND
                                                     user_messages.read = 0');

        return $count->count();
    }

    public function actionReadMessages($message_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if(!$this->checkAccessAction(false, $message_id)) {
            return false;
        }

        $message = UserMessages::find()->where(['message_id' => $message_id, 'recipient' => \Yii::$app->user->id, 'read' => 0])->one();

        if(!$message) {
            return false;
        }

        $message->read = 1;
        $message->update();

        return $message;
    }

}
