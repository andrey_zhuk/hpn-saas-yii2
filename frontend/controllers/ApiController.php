<?php

namespace frontend\controllers;
use app\models\Message;
use app\models\Notifications;
use app\models\OrdersRequest;
use app\models\PatientAssociateAgency;
use app\models\PlanCareRequest;
use app\models\UserMessages;
use common\models\LoginData;
use common\models\User;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use app\models\AjaxValidation;
use yii\widgets\ActiveForm;
use app\models\Chats;
use app\components\Controller;

class ApiController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSearch()
    {
        $return['facets'] = [];
        if (isset($_GET['c'])) $return['facets']['c'] = $_GET['c'];
        if (isset($_GET['sc'])) $return['facets']['sc'] = $_GET['sc'];

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $getParam = ['{{user}}.id', '{{user}}.short_id as ciphertext','first_name','last_name','gender','{{user}}.description',
            '{{user}}.country_code', '{{countries}}.countries_name', 'display_name','avatar_filename','job_title',
            'years_experience','biography', '{{favorite_users}}.save_date as favorite', '{{favorite_users}}.description as note'];
        $query = User::find()
            ->select($getParam)
            ->join('LEFT JOIN','auth_assignment','auth_assignment.user_id = user.id')
            ->join('LEFT JOIN','favorite_users','favorite_users.favorite_user_id = user.id and favorite_users.user_id = '.\Yii::$app->user->id)
            ->where(['auth_assignment.item_name' => 'designer'])
            ->joinWith(['countries'])
            ->with('expertises')
            ->limit(10)
            ->asArray()
            ->all();
//      var_dump('<pre>',$query,'</pre>');
        $arrayUserId = ArrayHelper::getColumn($query, 'id');
        $portfolioCount = AuthorPortfolios::getPortfolioCount($arrayUserId);
        $portfolioCount = ArrayHelper::map($portfolioCount, 'author_user', 'count_portfolio');
        foreach($query as $key => $row) {
            (isset($portfolioCount[$row['id']])) ? $query[$key]['count_portfolio'] = $portfolioCount[$row['id']] : $query[$key]['count_portfolio'] = 0;
        }
        /*
         *  experience filter count
         */
        $experience_count = User::find()
            ->select(['COUNT(*) as experience_count', 'experience_level'])
            ->join('LEFT JOIN','auth_assignment','auth_assignment.user_id = user.id')
            ->where(['auth_assignment.item_name' => 'designer'])
            ->groupBy('experience_level')
            ->asArray()
            ->all();

        $return['experience'] = ArrayHelper::map($experience_count, 'experience_level', 'experience_count');
        $return['freelancers'] = $query;
        $return['success'] = true;
        return $return;
    }

    public function actionSearchUser()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();
        $userKeyword = $request['search'];
        $user = User::find()
            ->select('*')
            ->where(['first_name' => $userKeyword])
            ->asArray()
            ->all();
        return $user;
    }

    public function actionSetSendingMessage()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();

        if($request) {
            $model = new Message();
            $model->message = addslashes($request['text']);
            $model->sender_id = \Yii::$app->user->id;
            $model->recepient_id = $request['recepient'];
            $model->date_create = time();
            if (!ActiveForm::validate($model)) {
                if ($model->save()) {
                    $data['result'] = 'success';
                    return $data;
                }
            } else {
                $data['result'] = 'error';
                $data['error_message'] = ActiveForm::validate($model);
                return $data;
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionSetRequestMedication()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();

        if($request) {
            $patient_id = (int)$request['patient'];
            $role = $this->userRole;

            $associate_agency = PatientAssociateAgency::find()
                ->where(['id_patient' => $patient_id, 'status_send' => PatientAssociateAgency::STATUS_ACCEPT])
                ->one();

            if ($role == 'physician' || ($associate_agency != NULL && $role == 'agency')) {
                $model = new OrdersRequest();
                $model->text = addslashes($request['text']);
                $model->id_patient = (int)$request['patient'];
                $model->id_agency = ($role == 'agency') ? \Yii::$app->user->id : $associate_agency->id_agency;
                $model->date_create = time();
                if ($role == 'physician')
                    $model->type = OrdersRequest::PHYSICIAN_SEND;
                elseif ($role == 'agency')
                    $model->type = OrdersRequest::AGENCY_SEND;

                if (!ActiveForm::validate($model)) {
                    if ($model->save()) {
                        $data['result'] = 'success';
                        return $data;
                    }
                } else {
                    $data['result'] = 'error';
                    $data['error_message'] = ActiveForm::validate($model);
                    return $data;
                }
            }
            else {
                $data['error_message'] = ["notrequest-text" => ["This agency is not permitted to request medicine."]];
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionSetRequestPocUpdate()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();

        if($request) {
            if (isset($request['patient'])) {
                $model = new PlanCareRequest();
                $model->id_patient = $request['patient'];
                $model->id_agency = \Yii::$app->user->id;
                $model->medications = $request['medications'];
                $model->dme_supplies = $request['dmesupplies'];
                $model->treatments = $request['treatments'];
                $model->discharge_plans = $request['dischargeplans'];
                $model->date_send = time();

                if (!ActiveForm::validate($model)) {
                    if($model->save()) {
                        $data['result'] = 'success';
                        return $data;
                    }
                } else {
                    $data['result'] = 'error';
                    $data['error_message'] = ActiveForm::validate($model);
                    return $data;
                }
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionGetActiveMessage()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $active_message = Message::getAllActiveMessage();
        $return['count'] = count($active_message);
        $return['data'] = [];

        if($return['count'] != 0) {
            foreach ($active_message as $message) {
                $param = $message->getAttributes();
                $param['user'] = $message->senderUser->getAttributes();
                $param['profile'] = $message->senderProfile->getAttributes();
                ($message->senderAgency != NULL) ?
                    $param['user_detail'] = $message->senderAgency->getAttributes()
                    : $param['user_detail'] = $message->senderPhysician->getAttributes();

                array_push($return['data'], $param);
            }
        }
        $return['result'] = 'success';

        return $return;
    }

    public function actionGetActiveNotifications()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $notification = PlanCareRequest::find()
            ->join('INNER JOIN','patients', 'patients.id = plan_care_request.id_patient and patients.id_physician = '.\Yii::$app->user->id)
//            ->where(['plan_care.id_patient' => 'designer'])
            ->with(['agency', 'profile'])
            ->all();

        $return['count'] = count($notification);
        $return['data'] = [];

        if($return['count'] != 0) {
            foreach ($notification as $detail) {
                $param = $detail->getAttributes();
                $param['agency'] = $detail->agency->getAttributes();
                $param['profile'] = $detail->profile->getAttributes();

                array_push($return['data'], $param);
            }
        }
        $return['result'] = 'success';

        return $return;
    }

    public function actionSetRejectPatient()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();

        if($post) {
            if (isset($post['id'])) {
                $patient_id = $post['id'];
                $model = PatientAssociateAgency::find()
                    ->where(['id_patient' => $patient_id, 'id_agency' => \Yii::$app->user->id])
                    ->with(['physician', 'patient'])
                    ->one();
                $model->date_accept = time();
                $model->status_send = PatientAssociateAgency::STATUS_REJECT;
                $model->comment = $post['comment'];
                $model->date_end_accept = time()+86400;

                $notification = new Notifications();
                $notification->id_from = \Yii::$app->user->id;
                $notification->id_to = $model->physician->id;
                $notification->text = "Patient ".$model->patient->name." was rejected.";
                $notification->link = "/patient/index?id=".$patient_id;
                $notification->date_send = time();

                if($model->save()) {
                    $notification->save();

                    $data['result'] = 'success';
                    return $data;
                }
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionSetAcceptPatient()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();

        if($post) {
            if (isset($post['id'])) {
                $patient_id = $post['id'];
                $model = PatientAssociateAgency::find()
                    ->where(['id_patient' => $patient_id, 'id_agency' => \Yii::$app->user->id])
                    ->with(['physician', 'patient'])
                    ->one();
                $model->date_accept = time();
                $model->status_send = PatientAssociateAgency::STATUS_ACCEPT;

                $notification = new Notifications();
                $notification->id_from = \Yii::$app->user->id;
                $notification->id_to = $model->physician->id;
                $notification->text = "Patient ".$model->patient->name." is accepted for treatment";
                $notification->link = "/patient/index?id=".$patient_id;
                $notification->date_send = time();
                if($model->save()) {
                    $notification->save();

                    $data['result'] = 'success';
                    return $data;
                }
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionSetAcceptOrder()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();

        if($post) {
            if (isset($post['id'])) {
                $order_patient = OrdersRequest::findOne($post['id']);
                $order_patient->date_accept = time();

                if($order_patient->save()) {
                    $data['data'] = ['text' => $order_patient->text, 'date' => date('m/d/Y', $order_patient->date_accept)];
                    $data['result'] = 'success';
                    return $data;
                }
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionSetAcceptOrderPhysician()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();

        if($post) {
            if (isset($post['id'])) {
                $order_patient = OrdersRequest::findOne($post['id']);
                $order_patient->date_resend = time();

                if($order_patient->save()) {
                    $data['data'] = ['text' => $order_patient->text, 'date' => date('m/d/Y', $order_patient->date_resend)];
                    $data['result'] = 'success';
                    return $data;
                }
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionLoadNotifications()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();

        if ($post) {
            $data = Notification::find()->where([
                'user_id' => $post['userId'],
                'visited' => Notification::NOT_VISITED,
            ])->all();
            return $data;
        }
        $data['result'] = 'error';
        return $data;
    }

    public function actionVisitNotification()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->post();

        if ($post) {
            $notification = Notification::findOne($post['notification']);
            $notification->visited = Notification::VISITED;
            if ($notification->save()) {
                $data['result'] = 'success';
                return $data;
            }
        }
        $data['result'] = 'error';
        return $data;
    }

    /**
     * Load history chat
     *
     * @return mixed
     */
    public function actionLoadChatHistory($chat_id, $limit = 10, $message_id = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $new_messages = MessageController::actionGetCountUnreadMessages(false, $chat_id);
        $first_unread_message = UserMessages::find()->select('message_id')
                                            ->where(['recipient' => \Yii::$app->user->id, 'read' => 0])
                                            ->innerJoin('message', 'message.id = user_messages.message_id AND
                                                        message.chat_id = ' . $chat_id)
                                            ->one();
        $chat = Chats::find()->where(['id' => $chat_id])
                ->with(['members', 'messages' => function ($query) use ($message_id, $limit, $chat_id, $new_messages, $first_unread_message){
                    if($message_id) {
                        $query->where('message.id < :id', [':id' => $message_id]);
                    }
                    /* if is unread messages then show them */
                    if($new_messages > $limit){
                        $query->where('message.id >= :id', [':id' => $first_unread_message->message_id]);
                    } else {
                        $query->limit($limit);
                    }
                    $query->orderBy(['message.id' => SORT_DESC])->all();
                }])
                ->asArray()->one();
        $chat['messages'] = array_reverse($chat['messages']);
        $chat['new_messages'] = $new_messages;

        foreach ($chat['members'] as $user_detail) {
            $current_user = ArrayHelper::toArray(LoginData::findOne($user_detail['user']['id'])->userData);
            $current_user['short_id'] = $user_detail['user']['short_id'];
            $chat['members']['detail'][$user_detail['user']['id']] = $current_user;
        }
        return $chat;
    }
}
