<?php
namespace common\models;

use common\models\LoginData;
use Yii;
use yii\base\Model;
use yii\debug\models\search\Log;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    // use this scenario to make sure Physician is logging in (not Agency)
    // it's used for example in modal login form during contest creation (step 3)
    const SCENARIO_PHYSICIAN_LOGIN = 'scenario.physicianLogin';
    const SCENARIO_AGENCY_LOGIN = 'scenario.agencyLogin';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

            ['email', 'isPhysician', 'on' => [self::SCENARIO_PHYSICIAN_LOGIN]],
            ['email', 'isAgency', 'on' => [self::SCENARIO_AGENCY_LOGIN]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'E-Mail '),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    //checks if user account is Physician type
    public function isPhysician($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if($user->type != LoginData::TYPE_PHYSICIAN)
                $this->addError($attribute, Yii::t("app", "This account type is not Physician"));
        }
    }

    //checks if user account is Agency type
    public function isAgency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if($user->type != LoginData::TYPE_AGENCY)
                $this->addError($attribute, Yii::t("app", "This account type is not Agency"));
        }
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
//            var_dump($this->getUser()); die();
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = LoginData::findByEmail($this->email);

//            if (! $checkUser)
//                return false;
//            if (Yii::$app->params['currentInstance'] == 'frontend') {
//                $this->_user = $checkUser->type === LoginData::TYPE_ADMIN ? false : $checkUser; }
//            elseif (Yii::$app->params['currentInstance'] == 'backend')
//                $this->_user = $checkUser->type === LoginData::TYPE_ADMIN ? $checkUser : false;
        }

        return $this->_user;
    }
}
