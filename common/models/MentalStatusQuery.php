<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MentalStatus]].
 *
 * @see MentalStatus
 */
class MentalStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MentalStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MentalStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}