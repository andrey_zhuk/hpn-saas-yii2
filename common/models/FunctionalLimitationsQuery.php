<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FunctionalLimitations]].
 *
 * @see FunctionalLimitations
 */
class FunctionalLimitationsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FunctionalLimitations[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FunctionalLimitations|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}