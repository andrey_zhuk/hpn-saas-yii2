<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id
 * @property string $countries_name
 * @property string $countries_iso_code_2
 * @property string $countries_iso_code_3
 * @property integer $address_format_id
 * @property integer $active
 * @property string $countries_currencyid
 * @property string $tax_region
 * @property double $tax_vat
 * @property integer $phone_prefix
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_format_id', 'active', 'phone_prefix'], 'integer'],
            [['tax_region'], 'required'],
            [['tax_vat'], 'number'],
            [['countries_name'], 'string', 'max' => 64],
            [['countries_iso_code_2'], 'string', 'max' => 2],
            [['countries_iso_code_3', 'countries_currencyid', 'tax_region'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'countries_name' => Yii::t('app', 'Countries Name'),
            'countries_iso_code_2' => Yii::t('app', 'Countries Iso Code 2'),
            'countries_iso_code_3' => Yii::t('app', 'Countries Iso Code 3'),
            'address_format_id' => Yii::t('app', 'Address Format ID'),
            'active' => Yii::t('app', 'Active'),
            'countries_currencyid' => Yii::t('app', 'Countries Currencyid'),
            'tax_region' => Yii::t('app', 'Tax Region'),
            'tax_vat' => Yii::t('app', 'Tax Vat'),
            'phone_prefix' => Yii::t('app', 'Phone Prefix'),
        ];
    }
}
