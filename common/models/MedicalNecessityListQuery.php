<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MedicalNecessityList]].
 *
 * @see MedicalNecessityList
 */
class MedicalNecessityListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MedicalNecessityList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MedicalNecessityList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}