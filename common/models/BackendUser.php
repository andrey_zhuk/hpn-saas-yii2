<?php

namespace common\models;

use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "backend_user".
 *
 * @property integer $id
 * @property string $name
 * @property string $gender
 * @property string $avatar_filename
 */
class BackendUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'backend_user';
    }

    public static function shortClassName() {
        return 'BackendUser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['gender', 'avatar_filename'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'gender' => Yii::t('app', 'Gender'),
            'avatar_filename' => Yii::t('app', 'Avatar Filename'),
        ];
    }

    public function getLoginData() {
        return $this->hasOne(LoginData::className(), ['id' => 'id']);
    }

    public function getLogo($type = 'original') {
        if ($this->avatar_filename == null) {
            $logo_filename = Yii::$app->request->baseUrl . "/app/images/noavatar.png";
        } else {
            if ($type == 'original')
                $logo_filename = '/app/images/avatar/' . $this->avatar_filename;
            elseif ($type == 'square') {
                $path_parts = pathinfo($this->avatar_filename);
                $pieces = explode(".", basename($this->avatar_filename));
                $logo_filename = '/app/images/avatar/' . $pieces[0] . '_square.' . $path_parts['extension'];
            }
        }

        return $logo_filename;
    }

    public function getUser()
    {
        return $this->hasOne(LoginData::className(), ['id' => 'id']);
    }

    public function setUserAvatar($files) {
        if (file_exists($files['tmp_name']['avatar_filename'])) {

            if (!file_exists(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"]))
                mkdir(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"], 0777);
            $image = Yii::$app->image->load($files['tmp_name']['avatar_filename']);

            $size = getimagesize($files['tmp_name']['avatar_filename']);
            $crop_size = ($size[0] > $size[1]) ? $size[1] : $size[0];

            $fileinfo = pathinfo($files['name']['avatar_filename']);
            $file_name = md5_file($files['tmp_name']['avatar_filename']) . '.' . $fileinfo['extension'];
            $file_name_square = md5_file($files['tmp_name']['avatar_filename']).'_square'.'.'.$fileinfo['extension'];
            $file_name_full = Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"].$file_name;

            $status = $image->save($file_name_full);

            Image::thumbnail(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"].$file_name, $crop_size, $crop_size)
                ->save(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"].$file_name_square, ['quality' => 100]);

            if ($status)
                return $file_name;
        }
    }
}
