<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AgencysSearch represents the model behind the search form about `app\models\Agencys`.
 */
class AgencysSearch extends Agencys
{
    public $short_id;
//    public $admin_name;
//    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'admin_name', 'phone', 'phone_fax', 'address', 'agency_email', 'skilled_nursing', 'physical_therapy',
                'speech_language_therapy', 'occupational_therapy', 'home_health_aide', 'medical_social_services', 'summary',
                'avatar_filename', 'short_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        $query = PhysicianRecomendationAgency::find()
//            ->where(['id_physician' => \Yii::$app->user->id])->joinWith(['agency']);
        if (isset($params["AgencysSearch"]["name"]) && $params["AgencysSearch"]["name"] != '')
            $query = Agencys::find();
        else
            $query = Agencys::find()->innerJoinWith(['physicianRecomAgency']);

        $query->joinWith(['user']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

//        $dataProvider->sort->attributes['agency_name'] = [
//            'asc' => ['agencys.name' => SORT_ASC],
//            'desc' => ['agencys.name' => SORT_DESC],
//        ];
//
//        $dataProvider->sort->attributes['admin_name'] = [
//            'asc' => ['agencys.admin_name' => SORT_ASC],
//            'desc' => ['agencys.admin_name' => SORT_DESC],
//        ];
//
//        $dataProvider->sort->attributes['phone'] = [
//            'asc' => ['agencys.phone' => SORT_ASC],
//            'desc' => ['agencys.phone' => SORT_DESC],
//        ];
//
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
//
//        $query->andFilterWhere([
//            'id_user' => $this->id_user,
//        ]);
//
//        $query->andFilterWhere(['like', 'agencys.name', $this->agency_name])
//            ->andFilterWhere(['like', 'agencys.admin_name', $this->admin_name])
//            ->andFilterWhere(['like', 'agencys.phone', $this->phone]);

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'admin_name', $this->admin_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_fax', $this->phone_fax])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'agency_email', $this->agency_email])
            ->andFilterWhere(['like', 'skilled_nursing', $this->skilled_nursing])
            ->andFilterWhere(['like', 'physical_therapy', $this->physical_therapy])
            ->andFilterWhere(['like', 'speech_language_therapy', $this->speech_language_therapy])
            ->andFilterWhere(['like', 'occupational_therapy', $this->occupational_therapy])
            ->andFilterWhere(['like', 'home_health_aide', $this->home_health_aide])
            ->andFilterWhere(['like', 'medical_social_services', $this->medical_social_services])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'avatar_filename', $this->avatar_filename])
            ->andFilterWhere(['like', 'short_id', $this->short_id]);

        return $dataProvider;
    }
}
