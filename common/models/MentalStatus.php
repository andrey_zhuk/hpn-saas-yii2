<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mental_status".
 *
 * @property integer $id
 * @property string $num
 * @property string $name
 */
class MentalStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mental_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num', 'name'], 'required'],
            [['num'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'num' => Yii::t('app', 'Num'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return MentalStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MentalStatusQuery(get_called_class());
    }
}
