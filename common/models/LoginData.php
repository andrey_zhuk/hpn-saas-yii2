<?php

namespace common\models;

use frontend\controllers\PhysicianController;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "login_data".
 *
 * @property integer $id
 * @property string $email
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $logins
 * @property integer $last_login
 * @property integer $status
 * @property string $short_id
 */
class LoginData extends \yii\db\ActiveRecord implements IdentityInterface
{
    const TYPE_ADMIN = 1;
    const TYPE_PHYSICIAN = 2;
    const TYPE_AGENCY = 3;

    const TYPE_ADMIN_NAME = 'admin';
    const TYPE_PHYSICIAN_NAME = 'physician';
    const TYPE_AGENCY_NAME = 'agency';

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_DELETED = 20;
    const STATUS_BLOCKED = 30;

    const SCENARIO_STEP_1 = 'scenario.changePassword';

    public $currentPassword;
    public $passwordNew;
    public $verifyPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login_data';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password_hash', 'auth_key'], 'required'],
            [['created_at', 'updated_at', 'logins', 'last_login', 'status'], 'integer'],
            [['email', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['short_id'], 'string', 'max' => 50],
            [['email'], 'unique'],

            [['currentPassword', 'passwordNew', 'verifyPassword'], 'required', 'on' => [self::SCENARIO_STEP_1]],
            ['currentPassword', 'string', 'min' => 6, 'on' => [self::SCENARIO_STEP_1]],
            ['currentPassword', 'validateCurrentPassword', 'on' => [self::SCENARIO_STEP_1]],
            [['passwordNew', 'verifyPassword'], 'required', 'on' => [self::SCENARIO_STEP_1]],
            [['passwordNew', 'verifyPassword'], 'string', 'min' => 6, 'on' => [self::SCENARIO_STEP_1]],
            ['verifyPassword', 'compare', 'compareAttribute'=>'passwordNew', 'message' => Yii::t('app', 'Retype Password is incorrect.'), 'on' => [self::SCENARIO_STEP_1]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'logins' => Yii::t('app', 'Logins'),
            'last_login' => Yii::t('app', 'Last Login'),
            'status' => Yii::t('app', 'Status'),
            'short_id' => Yii::t('app', 'Short ID'),
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token))
            return null;

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token))
            return false;

        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function getUserData() {

        return $this->hasOne($this->type === self::TYPE_PHYSICIAN ? Physician::className()
            : ($this->type === self::TYPE_AGENCY ? Agencys::className()
                : BackendUser::className()), ['id' => 'id']);
    }

    public function getAllAttributes() {
        return array_merge($this->attributes, $this->userData->attributes);
    }

    public function getRole() {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])
            ->viaTable('auth_assignment', ['user_id' => 'id'])
            ->one();
    }

    public function getRelationRole() {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])
            ->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    public static function findByRole($role) {
        $query = static::find()
            ->with('userData')
            ->join('LEFT JOIN','auth_assignment','auth_assignment.user_id = id')
            ->where(['auth_assignment.item_name' => $role])
            ->orderBy('created_at DESC')
            ->all();

        return $query;
    }

    public function getStatus() {
        return $this->status === self::STATUS_ACTIVE ? Yii::t('app', 'Active')
            : ($this->status === self::STATUS_DELETED ? Yii::t('app', 'Deleted')
                : ($this->status === self::STATUS_BLOCKED ? Yii::t('app', 'Blocked')
                    : Yii::t('app', 'Inactive')));
    }

    public function getPortfolioUrl() {
        return Url::to('/profile/show/' . $this->short_id);
    }


    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function validateCurrentPassword() {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError("currentPassword", "Current password incorrect");
        }
    }

    public static function findByShortId($shortId) {
        return self::findOne([
            'short_id' => $shortId,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

//    public function afterSave($insert, $changedAttributes) {
//        if ($this->type !== self::TYPE_ADMIN) {
//            $elastic = $insert ? new \common\models\elastic\User() :
//                \common\models\elastic\User::find()->where(['id' => $this->id])->one();
//
//            if(!$elastic)
//                $elastic = new \common\models\elastic\User();
//
//            $elastic->setAttributes($this->attributes, false);
//            $elastic->save();
//        }
//
//        parent::afterSave($insert, $changedAttributes);
//    }
}
