<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'uploadUserFileDir' => '/web/app/images/avatar/',
    'uploadJobsFileDir' => '/app/images/jobs_files/',
    'uploadContestAttachementsDir' => '/app/images/contests/attachements/',
];
