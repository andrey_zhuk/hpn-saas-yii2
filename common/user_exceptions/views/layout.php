<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\user_exceptions\classes\RenderedUserException;

/* @var $this yii\web\View */
/* @var $exception RenderedUserException */

$this->title = $exception->getName();
Yii::$app->layout = 'clean';
?>


<?= $this->renderFile($exception->path, [
    'exception' => $exception
]); ?>





<?php if (YII_ENV === 'dev'): ?>

    <div class="debug" style="margin-top: 200px">
        <hr/>
        <h2>DEBUG</h2>
        <div>
             <label for="">Message:</label> <?= $exception->getMessage() ?>
        </div>
        <div>
            <label for="">File:</label> <?= $exception->getFile() ?>
        </div>
        <div>
            <label for="">Line:</label> <?= $exception->getLine() ?>
        </div>
    </div>

    <pre><?= $exception->getTraceAsString() ?></pre>
<?php endif; ?>
