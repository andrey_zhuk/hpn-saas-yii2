<?php
use common\user_exceptions\classes\RenderedUserException;
    
/* @var $exception RenderedUserException */
?>

<h1>Example exception </h1>
<p>
    <?= Yii::t("user-exception", "Anything here") ?> <br/>
    <?= $exception->exceptionMessage ?>
</p>
