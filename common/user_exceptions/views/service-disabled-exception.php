<?php
    use yii\helpers\Url;
?>

<div class="row">
    <div class="col-sm-6">
        <h1><?= Yii::t('user-exception', 'Service disabled'); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <p><?= Yii::t('user-exception', 'This function is temporary disabled. Please try again later or contact support for more information'); ?></p>
        <p><a href="#" onclick="window.history.back();" class="btn btn-primary"><?= Yii::t('user-exception', 'Go back'); ?></a></p>
    </div>
</div>