<?php
namespace common\user_exceptions\classes;




class UserNotFoundException extends RenderedUserException {
 

    public function getName() 
    {
        return "User not found";
    }
}
