<?php
namespace common\user_exceptions\classes;


class SignupDisabledException extends RenderedUserException {
  
    public $exceptionMessage = "Registration is temporarily disabled. Please try again later.";

    public function getName() 
    {
        return "Signup disabled";
    }
}
