<?php
namespace common\user_exceptions\classes;




class CategoryNotFoundException extends RenderedUserException {
 
    
    public function getName() 
    {
        return "Category not found";
    }
}
