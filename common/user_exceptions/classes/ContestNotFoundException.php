<?php
namespace common\user_exceptions\classes;




class ContestNotFoundException extends RenderedUserException {
 
    
    public function getName() 
    {
        return "Contest not found";
    }
}
