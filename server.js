var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');

var typing_users = [];
var users = [];


server.listen(8890);
io.on('connection', function (socket) {

    /* initialize user */
    socket.on('init user', function (user) {
        console.log('User ' + user.user_id +  '  has init');
        users[user.user_id] = socket;
    });

    /* join user to private chat */
    socket.on('initChat', function(room, user){
        socket.join(room);
        console.log(user.name + " has joined room " + room);
    });

    /* send request medication */
    socket.on('send request medication', function(data){
        users[data.id_user].emit('send-request-medication', data);
    });

    /* update message */
    socket.on('update chat message', function(data){
        io.sockets.in(data.chat_id).emit('update-chat-message', data);
    });

    socket.on("send chat message", function(data) {
        var ids = [];

        console.log('send chat message');
        data.chat.members.forEach(function(user, index, array){
            if(users[user.user_id]) {
                var roomClients = io.sockets.adapter.rooms[data.chat_id];
                if(roomClients) {
                    ids = Object.keys(roomClients.sockets);
                }

                if(ids.indexOf(users[user.user_id].id) == -1 && user.user_id !=  data.sender.sender){
                    users[user.user_id].emit('send-chat-message', data);
                }
            }
        });
        io.sockets.in(data.chat_id).emit('send-chat-message', data);
    });


    socket.on("typing", function(data) {
        console.log(data);
        /* add typing user to arr */
        if (data.typing) {
            typing_users.push(data.user);
        } else {
            /* delete typing user from arr */
            typing_users = [];
        }
        /* send message to all clients exept sender */
        socket.broadcast.to(data.chat_id).emit("is-typing", {typing: data.typing, users: typing_users, chat_id: data.chat_id});
    });

    socket.on('disconnect', function() {
        //redisClientLike.quit();
        console.info('Client gone (id=' + socket.id + ').');
    });

});

