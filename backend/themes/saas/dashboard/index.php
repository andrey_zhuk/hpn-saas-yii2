<?php
use yii\helpers\Url;

$this->title = Yii::t('app', 'Dashboard');
//$this->registerCssFile(Yii::$app->request->baseUrl.'/css/content.css');
//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/content.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="row">
    <div class="col-sm-4">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon pink"><i class="fa fa-users"></i></span>
            <div class="mini-stat-info">
                <span><?= $patients; ?></span>
                <?= Yii::t('app', 'Patients'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon green"><i class="fa fa-paint-brush"></i></span>
            <div class="mini-stat-info">
                <span><?= $agencys; ?></span>
                <?= Yii::t('app', 'Agencys'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon green"><i class="fa fa-paint-brush"></i></span>
            <div class="mini-stat-info">
                <span><?= $physicians; ?></span>
                <?= Yii::t('app', 'Physicians'); ?>
            </div>
        </div>
    </div>
</div>

<div class="event-calendar clearfix">
    <div class="col-lg-7 calendar-block">
        <div class="cal1">
            <div class="clndr">

            </div>
        </div>
    </div>
    <div class="col-lg-5 event-list-block pin">
        <div class="profile-nav alt">
            <section class="panel">
                <div class="user-heading alt clock-row terques-bg">
                    <h1><?= $currentDate->format('F d');?></h1>
                    <p class="text-left"><?= $currentDate->format('Y, l');?></p>
                    <p class="text-left"><?= $currentDate->format('g:i A');?></p>
                </div>
                <ul id="clock">
                    <li id="sec"></li>
                    <li id="hour"></li>
                    <li id="min"></li>
                </ul>
            
            </section>
        </div>
    </div>
</div>
