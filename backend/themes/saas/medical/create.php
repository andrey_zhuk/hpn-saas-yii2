<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MedicalNecessityList */

$this->title = Yii::t('app', 'Create Medical Necessity List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Medical Necessity Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="panel medical-necessity-list-create col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
