<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Agencys */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Agencys',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<section class="panel agencys-update col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</section>
