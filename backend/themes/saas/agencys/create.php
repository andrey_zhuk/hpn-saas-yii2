<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Agencys */

$this->title = Yii::t('app', 'Create Agencys');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="panel agencys-create col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</section>
