<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \common\models\Patients;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PatientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Patients');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patients-index">
    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'physician_name',
            'value' => 'physician.name',
            'filter' => $array_physician
        ],
        'name',
        'birthday',
        'phone',
        'address',
        'email:email',
        [
            'attribute'=>'gender',
            'vAlign'=>'middle',
            'value'=>function ($data) {
                $arr = Patients::getGender();
                return ($data->gender) ? $arr[$data->gender] : NULL;
            },
            'filter' => Patients::getGender()
        ],
        // 'photo',
        // 'ss',
        // 'emergency_contact_name',
        // 'emergency_contact_phone',
        // 'emergency_contact_address',
        // 'insurance_plan1',
        // 'insurance_policy1',
        // 'insurance_plan2',
        // 'insurance_policy2',
        // 'clinical_findings',
        // 'reason_patient_homebound',
        // 'date_create',
        // 'status_care',
        // 'date_care_end',

//            ['class' => 'yii\grid\ActionColumn'],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel'=>[
            'heading'=>'<h3 class="panel-title"><i class="fa fa-group"></i> '. $this->title.'</h3>',
            'type'=>'success'
        ],
        'pjax'=>true,
        'columns' => $gridColumns,
        'toolbar' => [
            [
                'content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], [
                        'class' => 'btn btn-default btn-sm',
                        'title' => Yii::t('app', 'Reset Grid')
                    ]),
            ],
            '{toggleData}'
        ],
        'toggleDataOptions' => [
            'all' => [
                'icon' => 'resize-full',
                'class' => 'btn btn-default btn-sm',
            ]
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'striped'=>true,
    ]); ?>

</div>
