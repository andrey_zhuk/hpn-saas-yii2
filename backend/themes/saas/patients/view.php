<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Patients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Patients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patients-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_physician',
            'name',
            'birthday',
            'phone',
            'address',
            'email:email',
            'gender',
            'photo',
            'ss',
            'emergency_contact_name',
            'emergency_contact_phone',
            'emergency_contact_address',
            'insurance_plan1',
            'insurance_policy1',
            'insurance_plan2',
            'insurance_policy2',
            'clinical_findings',
            'reason_patient_homebound',
            'date_create',
            'status_care',
            'date_care_end',
        ],
    ]) ?>

</div>
