<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Patients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patients-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'id_physician')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->dropDownList([ 'M' => 'M', 'F' => 'F', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ss')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emergency_contact_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emergency_contact_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emergency_contact_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_plan1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_policy1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_plan2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_policy2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clinical_findings')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reason_patient_homebound')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_create')->textInput() ?>

    <?= $form->field($model, 'status_care')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'date_care_end')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
