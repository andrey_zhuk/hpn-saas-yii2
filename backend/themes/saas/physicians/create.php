<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Physician */

$this->title = Yii::t('app', 'Create Physician');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Physicians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="panel physician-create col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</section>
