<?php
use yii\helpers\Url;
use common\models\LoginData;
use backend\assets\AngularAsset;

$this->title = Yii::t('app', 'Associate users');
$this->params['breadcrumbs'] = [
    ['label' => 'Associate user'],
];

AngularAsset::register($this);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/user.css');
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/associate_user.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<script>
    var physician = <?= json_encode($physicians); ?>;
</script>
<style>
    body {
        padding: 15px;
    }

    .select2 > .select2-choice.ui-select-match {
        /* Because of the inclusion of Bootstrap */
        height: 29px;
    }

    .selectize-control > .selectize-dropdown {
        top: 36px;
    }
    /* Some additional styling to demonstrate that append-to-body helps achieve the proper z-index layering. */
    .select-box {
        background: #fff;
        position: relative;
        z-index: 1;
    }
    .alert-info.positioned {
        margin-top: 1em;
        position: relative;
        z-index: 10000; /* The select2 dropdown has a z-index of 9999 */
    }
</style>
<section ng-app="users" ng-controller="main">
    <div class="panel col-sm-4 users panel-body ng-cloak">
        <ui-select ng-model="user.selected" ng-change="changeUser(user.selected)" theme="selectize" title="Choose a country">
            <ui-select-match placeholder="<?= Yii::t('app', 'Select or search a physicians in the list...') ?>">{{$select.selected.name}}</ui-select-match>
            <ui-select-choices repeat="user in users | filter: $select.search">
                <span ng-bind-html="user.name | highlight: $select.search"></span>
                (<small ng-bind-html="user.phone | highlight: $select.search"></small>)
            </ui-select-choices>
        </ui-select>
    </div>
    <div class="col-sm-8">
        <!--notification start-->
        <section class="panel" ng-if="user.selected">
            <header class="panel-heading">
                <?= Yii::t('app', 'Associate agency for') ?> <strong>{{user.selected.name}}</strong>
            </header>
            <div class="panel-body">
                <div class="alert alert-info clearfix" ng-repeat="associate in associate_user">
                    <button class="btn btn-round btn-danger" type="button" ng-click="deleteAssociateAgency(associate)"><i class="fa fa-trash-o"></i></button>
                    <div class="notification-info">
                        <ul class="clearfix notification-meta">
                            <li class="pull-left notification-sender"><span><a href="#">{{associate.name}}</a></span></li>
                            <li class="pull-right notification-time">{{associate.agency_email}}</li>
                        </ul>
                        <ng-bind-html ng-bind-html="associate.summary"></ng-bind-html>
                    </div>
                </div>

                <div class="todo-action-bar" ng-if="all_nonassociate_user.length > 0">
                    <div class="row">
                        <div class="col-xs-4 btn-add-task">
                            <ui-select ng-model="associate_user.selected" theme="selectize" title="Choose a country">
                                <ui-select-match placeholder="<?= Yii::t('app', 'Select or search a physicians in the list...') ?>">{{$select.selected.name}}</ui-select-match>
                                <ui-select-choices repeat="associate_user in all_nonassociate_user | filter: $select.search">
                                    <span ng-bind-html="associate_user.name | highlight: $select.search"></span>
                                    (<small ng-bind-html="associate_user.phone | highlight: $select.search"></small>)
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-sm-2 btn-add-task">
                            <button type="submit" class="btn btn-default btn-primary" ng-click="addAssociateAgency(associate_user.selected)"><i class="fa fa-plus"></i> <?= Yii::t('app', 'Add agency') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--notification end-->
    </div>
</section>
