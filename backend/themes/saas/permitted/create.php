<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ActivitiesPermitted */

$this->title = Yii::t('app', 'Create Activities Permitted');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activities Permitteds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="panel activities-permitted-create col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
