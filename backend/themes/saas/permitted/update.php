<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ActivitiesPermitted */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Activities Permitted',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activities Permitteds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<section class="panel activities-permitted-update col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
