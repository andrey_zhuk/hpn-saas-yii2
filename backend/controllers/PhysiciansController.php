<?php

namespace backend\controllers;

use common\models\PhysicianRecomendationAgency;
use common\models\Agencys;
use common\models\AjaxValidation;
use common\models\Countries;
use common\models\LoginData;
use thamtech\uuid\helpers\UuidHelper;
use Yii;
use common\models\Physician;
use backend\models\PhysicianSearch;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use backend\components\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PhysiciansController implements the CRUD actions for Physician model.
 */
class PhysiciansController extends Controller
{
    use AjaxValidation;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'associate-agency', 'get-associate-user',
                            'delete-associate-agency', 'add-associate-agency', 'active-block-user'],
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Physician models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhysicianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Physician model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Physician model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Physician();
        $loginData = new LoginData();

        $data['countries'] = Countries::find()->orderBy('countries_name')->all();
        $data['detail'] = $loginData;

        // ajax validator
        $this->performAjaxValidation($loginData);
        $this->performAjaxValidation($model);

        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $model->load($post);
            $loginData->load($post);

            $password = Yii::$app->security->generateRandomString(12);
            $loginData->setPassword($password);
            $loginData->auth_key = Yii::$app->security->generateRandomString();

            if (!ActiveForm::validate($loginData)) {
                $loginData->type = LoginData::TYPE_PHYSICIAN;
                $loginData->status = LoginData::STATUS_INACTIVE;
                $loginData->short_id = UuidHelper::uuid();

                if ($loginData->save()) {
                    $model->id = $loginData->id;
                    if (! empty($_FILES[$model->shortClassName()]['name']['avatar_filename']))
                        $model->avatar_filename = $model->setUserAvatar($_FILES[$model->shortClassName()]);

                    if (!ActiveForm::validate($model) && $model->save()) {
                        $auth = Yii::$app->authManager;
                        $authorRole = $auth->getRole(Physician::ROLE_NAME);
                        $auth->assign($authorRole, $loginData->id);

                        \Yii::$app->mailer->compose(['html' => 'activation-backend-html'], ['user' => $loginData, 'password' => $password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                            ->setTo($loginData->email)
                            ->setSubject('Activate account for ' . \Yii::$app->name)
                            ->send();

                        \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Your profile has been created'));
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        echo json_encode(ActiveForm::validate($model));
                        \Yii::$app->end();
                    }
                }
            } else {
                echo json_encode(ActiveForm::validate($loginData));
                \Yii::$app->end();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Updates an existing Physician model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $data['countries'] = Countries::find()->orderBy('countries_name')->all();

        // ajax validator
        $this->performAjaxValidation($model);

        if (\Yii::$app->request->isPost) {
            $oldAvatar = $model->avatar_filename;
            $post = \Yii::$app->request->post();
            $model->load($post);

            if (!ActiveForm::validate($model)) {
                if (! empty($_FILES[$model->shortClassName()]['name']['avatar_filename']))
                    $model->avatar_filename = $model->setUserAvatar($_FILES[$model->shortClassName()]);
                else
                    $model->avatar_filename = $oldAvatar;

                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Your profile has been updated'));
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                echo json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Deletes an existing Physician model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Physician model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Physician the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Physician::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAssociateAgency()
    {
        $physicians = Physician::find()->asArray()->all();

        return $this->render('associate_user', [
            'physicians' => $physicians
        ]);
    }

    public function actionGetAssociateUser()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();

        $user = Physician::find()->where(['id' => $request['id']])->with('physicianRecomendationAgency')->asArray()->one();

        $all_agency = ArrayHelper::index(Agencys::find()->asArray()->all(), 'id');
        $user_associate = ArrayHelper::index($user['physicianRecomendationAgency'], 'id');

        $array_diff_associate = array_diff_key($all_agency, $user_associate);

        if (count($user) > 0)
            return ['result' => 'success', 'userAssociate' => $user, 'allAgency' => array_values($array_diff_associate)];

        return ['result' => 'error'];
    }

    public function actionDeleteAssociateAgency()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();

        $user_deleted = PhysicianRecomendationAgency::find()->where(['id_physician' => $request['id_physician'], 'id_agency' => $request['id_agency']])->one();

        if ($user_deleted->delete()) {
            $user = Physician::find()->where(['id' => $request['id_physician']])->with('physicianRecomendationAgency')->asArray()->one();

            $all_agency = ArrayHelper::index(Agencys::find()->asArray()->all(), 'id');
            $user_associate = ArrayHelper::index($user['physicianRecomendationAgency'], 'id');

            $array_diff_associate = array_diff_key($all_agency, $user_associate);
            return ['result' => 'success', 'userAssociate' => $user, 'allAgency' => array_values($array_diff_associate)];
        }

        return ['result' => 'error'];
    }

    public function actionAddAssociateAgency()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request->post();

        $user_added = PhysicianRecomendationAgency::find()->where(['id_physician' => $request['id_physician'], 'id_agency' => $request['id_agency']])->one();
        if ($user_added)
            ['result' => 'exist'];
        $user_added = new PhysicianRecomendationAgency();
        $user_added->id_agency = $request['id_agency'];
        $user_added->id_physician = $request['id_physician'];

        if ($user_added->save()) {
            $user = Physician::find()->where(['id' => $request['id_physician']])->with('physicianRecomendationAgency')->asArray()->one();

            $all_agency = ArrayHelper::index(Agencys::find()->asArray()->all(), 'id');
            $user_associate = ArrayHelper::index($user['physicianRecomendationAgency'], 'id');

            $array_diff_associate = array_diff_key($all_agency, $user_associate);
            return ['result' => 'success', 'userAssociate' => $user, 'allAgency' => array_values($array_diff_associate)];
        }

        return ['result' => 'error'];
    }

    public function renderGrid() {
        $searchModel = new PhysicianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return self::renderPartial('_grid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActiveBlockUser()
    {
        $get = Yii::$app->request->get();
        $model = LoginData::findOne($get['id']);

        if($model->status == LoginData::STATUS_ACTIVE)
            $model->status = LoginData::STATUS_BLOCKED;
        else $model->status = LoginData::STATUS_ACTIVE;

        $model->save();

        echo self::renderGrid();
        \Yii::$app->end();
    }
}
