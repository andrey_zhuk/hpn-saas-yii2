<?php

namespace backend\controllers;

use common\models\LoginData;
use yii\web\Response;
use backend\components\Controller;
use Yii;

class ApiController extends Controller
{
    public function actionSendResetPasswordEmail() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        $user = LoginData::findOne([
            'status' => LoginData::STATUS_ACTIVE,
            'email' => $post['email'],
        ]);

        if ($user) {
            if (!LoginData::isPasswordResetTokenValid($user->password_reset_token))
                $user->generatePasswordResetToken();

            if ($user->save())
//                $trans = I18nSourceMessage::getMessageByMail(Yii::$app->language);
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($post['email'])
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
        }
        return false;
    }
}
