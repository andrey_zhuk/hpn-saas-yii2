<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use backend\components\Controller;
use common\models\LoginForm;


/**
 * Site controller
 */
class SiteController extends Controller
{
//    use AjaxValidationTrait;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public static function getMenuItems() {
        $controller = \Yii::$app->controller;
        return [
            ['label' => Yii::t('app', 'Home'), 'url' => Url::home(), 'active' => $controller->id == 'dashboard'],
            ['label' => Yii::t('app', 'Agencys'), 'url' => Url::to(['/agencys']), 'active' => $controller->id == 'agencys'],
            ['label' => Yii::t('app', 'Physicians'), 'url' => Url::to(['/physicians']), 'active' => "{$controller->id}/{$controller->action->id}" == 'physicians/index'],
            ['label' => Yii::t('app', 'Patients'), 'url' => Url::to(['/patients']), 'active' => $controller->id == 'patients'],
            ['label' => Yii::t('app', 'Edit Variables'), 'url' => '#', 'options' => ['class' => ($controller->id == 'permitted' || $controller->id == 'functional' || $controller->id == 'medical' || $controller->id == 'mental') ? 'open sub-menu' : 'sub-menu'],  'items' => [
                ['label' => Yii::t('app', 'Activities Permitted'), 'url' => Url::to(['/permitted']), 'options' => ['class' => 'sub'], 'active' => "{$controller->id}/{$controller->action->id}" == 'permitted/index'],
                ['label' => Yii::t('app', 'Functional Limitations'), 'url' => Url::to(['/functional']), 'options' => ['class' => 'sub'], 'active' => "{$controller->id}/{$controller->action->id}" == 'functional/index'],
                ['label' => Yii::t('app', 'Medical Necessity'), 'url' => Url::to(['/medical']), 'options' => ['class' => 'sub'], 'active' => "{$controller->id}/{$controller->action->id}" == 'medical/index'],
                ['label' => Yii::t('app', 'Mental Status'), 'url' => Url::to(['/mental']), 'options' => ['class' => 'sub'], 'active' => "{$controller->id}/{$controller->action->id}" == 'mental/index']
            ]],
            ['label' => Yii::t('app', 'Associate Agency'), 'url' => Url::to(['/physicians/associate-agency']),  'active' => "{$controller->id}/{$controller->action->id}" == 'physicians/associate-agency'],
            ['label' => Yii::t('app', 'Messages'), 'url' => Url::to(['/message']),  'active' => $controller->id == 'message'],
            ['label' => Yii::t('app', 'Reports'), 'url' => '#', 'options' => ['class' => $controller->id == 'reports' ? 'open' : ''],  'items' => [
            ]],
            ['label' => Yii::t('app', 'Manage users'), 'url' => Url::to(['/user/index']), 'active' => $controller->id == 'user'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest)
            return $this->render('login');

        return $this->goHome();
    }

    public function actionLogin()
    {
        $this->layout = 'admin';

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = \Yii::createObject(LoginForm::className());

//        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
