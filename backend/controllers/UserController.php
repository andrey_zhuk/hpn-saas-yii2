<?php
namespace backend\controllers;

use common\models\AjaxValidation;
use common\models\AuthItem;
use common\models\LoginData;
use thamtech\uuid\helpers\UuidHelper;
use Yii;
use backend\components\Controller;
use common\models\BackendUser;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\ArrayHelper;

class UserController extends Controller
{
    use AjaxValidation;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'restore', 'block'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update' => ['post'],
                    'delete' => ['post'],
                    'restore' => ['post'],
                ],
            ],
        ];
    }

    function realActions() {
        return [
            'view' => 'index',
            'create' => 'create',
        ];
    }

    public function actionIndex()
    {
        $users = BackendUser::find()
            ->with('loginData.relationRole')
            ->asArray()
            ->all();
        $roles = ArrayHelper::map(AuthItem::getAdminRoles(), 'name', 'name');

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'New password has been saved.'));
        return $this->render('index', [
            'users' => $users,
            'roles' => $roles,
        ]);
    }

    public function actionBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = LoginData::findOne($_POST['id']);
        $user->status = LoginData::STATUS_BLOCKED;
        if ($user->save())
            return ['result' => 'success', 'userStatus' => LoginData::STATUS_BLOCKED];

        return ['result' => 'error'];
    }

    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = LoginData::findOne($_POST['id']);
        $user->status = LoginData::STATUS_DELETED;
        if ($user->save())
            return ['result' => 'success', 'userStatus' => LoginData::STATUS_DELETED];

        return ['result' => 'error'];
    }

    public function actionRestore()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = LoginData::findOne($_POST['id']);
        $user->status = LoginData::STATUS_ACTIVE;
        if ($user->save())
            return ['result' => 'success', 'userStatus' => LoginData::STATUS_ACTIVE];

        return ['result' => 'error'];
    }

    public function actionCreate($id = null)
    {
        $loginData = $id === null ? new LoginData() : LoginData::findOne($id);
        $userData = $id === null ? new BackendUser() : BackendUser::findOne($id);

        // ajax validator
        $this->performAjaxValidation($loginData);
        $this->performAjaxValidation($userData);

        if (Yii::$app->request->isPost) {
            $loginData->setAttributes($_POST['LoginData'], false);
            if ($loginData->isNewRecord) {
                $password = Yii::$app->security->generateRandomString(12);
                $loginData->setPassword($password);
                $loginData->auth_key = Yii::$app->security->generateRandomString();
                $loginData->type = LoginData::TYPE_ADMIN;
                $loginData->status = LoginData::STATUS_INACTIVE;
                $loginData->short_id = UuidHelper::uuid();
                if ($loginData->save()) {
                    $auth = Yii::$app->authManager;
                    $authorRole = $auth->getRole(LoginData::TYPE_ADMIN_NAME);
                    $auth->assign($authorRole, $loginData->id);
                }
            }
            if ($loginData->id) {
                $userData->setAttributes($_POST['BackendUser'], false);
                $userData->id = $loginData->id;
                if (! empty($_FILES['BackendUser']['name']['avatar_filename']))
                    $userData->avatar_filename = $userData->setUserAvatar($_FILES['BackendUser']);

                if ($userData->save()) {
                    if ($loginData->isNewRecord) {
                        \Yii::$app->mailer->compose(['html' => 'activation-backend-html'], ['user' => $loginData, 'password' => $password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                            ->setTo($loginData->email)
                            ->setSubject('Activate account for ' . \Yii::$app->name)
                            ->send();
                    }

                    return $this->redirect('/user/index');
                }
            }
        }

        return $this->render('create', [
            'loginData' => $loginData,
            'userData' => $userData,
            'editing' => $id === null,
        ]);
    }
}
