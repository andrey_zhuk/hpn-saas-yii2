
var placeSearch, autocomplete;

function initialize() {
    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById('autocomplete-city')),
        { types: ['(cities)'] });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        $('#autocomplete-city').val(autocomplete.getPlace().name);
    });
}

window.onload = function(){
    initialize();
};