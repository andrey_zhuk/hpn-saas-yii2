<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );

    public $css = [
        'sweetalert/dist/sweetalert.css'
    ];
    public $js = [
        'jquery.scrollTo/jquery.scrollTo.min.js',
        'jquery.slimscroll/jquery.slimscroll.min.js',
        'moment/moment.js',
        'sweetalert/dist/sweetalert.min.js',
        'underscore/underscore-min.js'
    ];
    public $depends = [
    ];
}
